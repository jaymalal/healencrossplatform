import { Component, OnInit } from '@angular/core';
import {  NavController } from '@ionic/angular';
import { ComplaintInfoService } from '../shared/Services/complaint-info.service';
import { StorageService } from '../shared/Services/storage.service';
import {  FormControl, FormBuilder , Validator, FormGroup} from '@angular/forms';
import { SessionManagerService } from '../shared/Services/session-manager.service';
import { Session } from '../shared/module/Session';
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(public navCtrl: NavController,
    private complaintInfoService: ComplaintInfoService,
    private storage: StorageService,
    private fb: FormBuilder,
    private sessionManager: SessionManagerService,
    private router: Router) { }

    patientId: any;
    buyId: any;
    sessionObj: Session;


  ngOnInit() {

    this.storage.getItem('patientId').then((response: any) => {
      console.log('patientId:', response);
      this.patientId = response;
    });

    this.storage.getItem('buyId').then((response: any) => {
      this.buyId = response;
  });
  }

  registerIssue() {
    // this.navCtrl.navigateForward('session-exercise');
   // this.navCtrl.navigateForward('test-exef-list');
   this.navCtrl.navigateForward('issue-type');
  }

  profileIssue() {
     this.navCtrl.navigateForward('profile');

  }

  getSessionInfoFromUserId() {

    this.sessionManager.GetSessionInfoFromId(this.patientId).subscribe((sessionInfo: any) => {

      this.sessionObj = sessionInfo.result;
      if ( sessionInfo.result.length > 0 ) {
        if (sessionInfo.result[sessionInfo.result.length - 1].currentStatus === 'COMPLETED') {
          this.createNewSession();
        } else {
          console.log('Session Info:', sessionInfo.result);
          this.storage.addItem('sessionId', sessionInfo.result[sessionInfo.result.length - 1].id);
          this.moveForTime();
        }
      } else {
          this.createNewSession();
       }
    });
  }


  createNewSession() {

      this.sessionManager.createNewSession(this.patientId, this.buyId).subscribe((sessionInfo: any) => {
      this.sessionObj = sessionInfo.result;
      this.storage.addItem('sessionId', this.sessionObj.id);
      console.log('session is created and session Id is' + sessionInfo.result.id);
      this.moveForTime();
      });
  }

  moveForTime() {
    this.sessionManager.GetSessionInfoFromId(this.patientId).subscribe((sessionInfo: any) => {

      this.sessionObj = sessionInfo.result;
      if ( sessionInfo.result.length > 1 ) {
        this.router.navigate(['previous-session-fb']);
      } else {
        this.router.navigate(['duration-time']);
      }
    });
  }


}
