import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AppResolverService } from './shared/Services/app-resolver.service';
import { AuthanticationGuardService } from './shared/Services/authantication-guard.service';

const routes: Routes = [
 // { path: '', redirectTo: 'sign-in', pathMatch: 'full' },
 { path: '', component: AppComponent },
  // { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'sign-up', loadChildren: './Authantication/sign-up/sign-up.module#SignUpPageModule' },
  { path: 'sign-in', loadChildren: './Authantication/sign-in/sign-in.module#SignInPageModule' },
  { path: 'profile', loadChildren: './userSetUpSetting/profile/profile.module#ProfilePageModule' },
  // tslint:disable-next-line: max-line-length
  { path: 'chief-complaint-detection', loadChildren: './ChiefComplaint/chief-complaint-detection/chief-complaint-detection.module#ChiefComplaintDetectionPageModule' },
  { path: 'area-selection', loadChildren: './ChiefComplaint/area-selection/area-selection.module#AreaSelectionPageModule' },
  { path: 'sub-area-selection', loadChildren: './ChiefComplaint/sub-area-selection/sub-area-selection.module#SubAreaSelectionPageModule' },
  { path: 'issue-type', loadChildren: './ChiefComplaint/issue-type/issue-type.module#IssueTypePageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'complaint1-a', loadChildren: './Questionairs/complaint1-a/complaint1-a.module#Complaint1APageModule' },
  { path: 'session-exercise', loadChildren: './ExerciseList/session-exercise/session-exercise.module#SessionExercisePageModule' },
  { path: 'duration-time', loadChildren: './ChiefComplaint/duration-time/duration-time.module#DurationTimePageModule' },
  { path: 'medical', loadChildren: './Questionairs/medical/medical.module#MedicalPageModule' },
  { path: 'video-display', loadChildren: './ExerciseList/video-display/video-display.module#VideoDisplayPageModule' },
  { path: 'exef-feedback', loadChildren: './ExerciseList/exef-feedback/exef-feedback.module#ExefFeedbackPageModule' },
{ path: 'pmexercise-feedback', loadChildren: './TestExercise/pmexercise-feedback/pmexercise-feedback.module#PMExerciseFeedbackPageModule'},
  { path: 'test-exef-list', loadChildren: './TestExercise/test-exef-list/test-exef-list.module#TestExefListPageModule' },
  { path: 'intro-video', loadChildren: './ExerciseList/intro-video/intro-video.module#IntroVideoPageModule' },
  // tslint:disable-next-line: max-line-length
  { path: 'previous-session-fb', loadChildren: './Questionairs/previous-session-fb/previous-session-fb.module#PreviousSessionFBPageModule' },
  { path: 'slfeedback', loadChildren: './TestExercise/slfeedback/slfeedback.module#SLFeedbackPageModule' },
  // tslint:disable-next-line: max-line-length
 // { path: 'common-video-display', loadChildren: './shared/Pages/common-video-display/common-video-display.module#CommonVideoDisplayPageModule' },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
 }
