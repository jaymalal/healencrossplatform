import { Component, OnInit } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';
import * as  Survey from 'survey-angular';
import { SurveyJsonManagerService } from '../../shared/Services/survey-json-manager.service';
import { StorageService } from '../../shared/Services/storage.service';
import {  NavController } from '@ionic/angular';

@Component({
  selector: 'app-complaint1-a',
  templateUrl: './complaint1-a.page.html',
  styleUrls: ['./complaint1-a.page.scss'],
})
export class Complaint1APage implements OnInit {
  @Input()
  surveyJSON: any;
  @Input()
  type: string;

  @Output() isComplete = new EventEmitter<any>();


 // Variables
  isLoading: any;
  surveyJsonData: any;
  questionaireId: any;
  patientId: any;
  complaintId: any;
  sessionId: any;

  constructor(private surveyJsonService: SurveyJsonManagerService,
    private storage: StorageService,
    public navCtrl: NavController) { }

  ngOnInit() {

    // get Observales current values
    this.storage.getItem('patientId').then((response: any) => {
      console.log(response);
      this.patientId = response;
    });

    this.storage.getItem('complaintId').then((response: any) => {
     // console.log(response);
      this.complaintId = response;
    });

    this.storage.getItem('sessionId').then((response: any) => {
      this.sessionId = response;
    });


    this.getSurveyJsonByName();
  }

  getSurveyJsonByName() {
    this.surveyJsonService.getQuestionnaireByName('1_A_Complaint').subscribe((Response: any) => {
      this.surveyJsonData = Response.json;
      this.questionaireId = Response.id;
      this.loadSurveyJsonAlt();
    });
  }

  loadSurveyJsonAlt() {
    const survey = new Survey.Model(this.surveyJsonData);
    survey.data = {
      self: this
    };
    survey.onComplete.add(this.sendDataToServerAlt);
    survey.onComplete.add(() => {
      survey.clear();
    });
    Survey.SurveyNG.render('surveyElement2', { model: survey });
  }
  sendDataToServerAlt(sendDataToServerAlt: any) {
    console.log(sendDataToServerAlt);

    sendDataToServerAlt.data.self.sendResponseToServerOn1AComplaint(sendDataToServerAlt.data);
  }

  sendResponseToServerOn1AComplaint(sendDataToServerAlt: any) {
    delete sendDataToServerAlt.self;
    const dto = {
        complaintExerciseId: 0,
        complaintId: this.complaintId,
        patientId: this.patientId,
        patientQuestionAnswer: sendDataToServerAlt,
        remedyPackageId: 1,
        sessionId: this.sessionId
    };
    this.surveyJsonService.SavePatientAnswer(dto, this.questionaireId).subscribe(response => {
      console.log(response);
      this.navigateBack();
    });
  }


  private navigateBack() {
    this.navCtrl.navigateForward('test-exef-list');
  }
}
