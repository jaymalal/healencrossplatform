import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Complaint1APage } from './complaint1-a.page';

describe('Complaint1APage', () => {
  let component: Complaint1APage;
  let fixture: ComponentFixture<Complaint1APage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Complaint1APage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Complaint1APage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
