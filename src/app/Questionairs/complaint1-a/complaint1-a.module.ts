import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Complaint1APage } from './complaint1-a.page';

const routes: Routes = [
  {
    path: '',
    component: Complaint1APage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Complaint1APage]
})
export class Complaint1APageModule {}
