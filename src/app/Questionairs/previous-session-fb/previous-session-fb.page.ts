import { Component, OnInit, OnDestroy } from '@angular/core';
import { SurveyJsonManagerService } from '../../shared/Services/survey-json-manager.service';
import * as  Survey from 'survey-angular';
import { StorageService } from '../../shared/Services/storage.service';
import { ComplaintExercise } from '../../shared/module/exercise';
import { ExerciseManagerService } from '../../ExerciseList/exercise-manager.service';
import { SessionManagerService } from '../../shared/Services/session-manager.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-previous-session-fb',
  templateUrl: './previous-session-fb.page.html',
  styleUrls: ['./previous-session-fb.page.scss'],
})
export class PreviousSessionFBPage implements OnInit, OnDestroy {

  constructor( private surveyJsonService: SurveyJsonManagerService,
    private storage: StorageService,
    private exerciseManager: ExerciseManagerService,
    private sessionManager: SessionManagerService,
    private router: Router) {

      this.exercise = this.exerciseManager.currentExercise;

    }
  private pageSubArray: Subscription[] = [];

  public set pageSub$(value: Subscription) {
    this.pageSubArray.push(value);
  }
  surveyJsonData: any;
  questionaireId: any;
  patientId: any;
  sessionId: any;
  complaintId: any;
  exercise: ComplaintExercise;

  ngOnInit() {

  this.storage.getItem('sessionId').then((response: any) => {
    this.sessionId = response;
   });

  this.storage.getItem('patientId').then((response: any) => {
    this.patientId = response;
  });

  this.storage.getItem('complaintId').then((response: any) => {
    this.complaintId = response;
  });
  this.loadSurveyJson();
}


  loadSurveyJson() {
    this.pageSub$ = this.surveyJsonService.getQuestionnaireByName('every_day_fb').subscribe((Response: any) => {
      this.surveyJsonData = Response.json;
      this.questionaireId = Response.id;
     // console.log(this.surveyJsonData);
      const survey = new Survey.Model('' + this.surveyJsonData);
      survey.data = {
        self: this,
      };
      survey.onComplete.add(this.sendDataToServerAlt);
      survey.onComplete.add(() => {
        survey.clear();
      });
      Survey.SurveyNG.render('surveyJsElementForDailyFB', { model: survey });
      });
  }

  sendDataToServerAlt(sendDataToServerAlt: any) {
    sendDataToServerAlt.data.self.savePatientAnswer(sendDataToServerAlt);
  }

  savePatientAnswer(surveyAns: any) {
    const result = surveyAns.data;
    delete result.self;
    const userResponse = {
      complaintId: this.complaintId,
      patientId: this.patientId,
      patientQuestionAnswer: result,
      remedyPackageId: 1,
      sessionId: this.sessionId
    };


    this.pageSub$ = this.sessionManager.saveFeedback(userResponse, this.questionaireId)
      .subscribe(
        (res: any) => {
          const execStatus = {
            cePr: 0,
            ocCounter: 0,
            postExerciseFeedbackId: res.result.id,
            progressionReps: 0,
            progressionSets: 0,
            repsBumped: true,
            repsDone: 0,
            sessionNumber: this.sessionId,
            setsBumped: true,
            setsDone: 0,
            slb: 0,
            soCounter: 0,
            soPainCounter: 0,
            status: 'DONE'
          };
         // console.log(res);
          this.router.navigate(['duration-time']);
        }
      );
  }

  ngOnDestroy(): void {
    try {
      this.pageSubArray.forEach(sub => sub.unsubscribe());
    } catch (error) {
      console.log('error while unsubscribing:', error);
    }
  }
}
