import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviousSessionFBPage } from './previous-session-fb.page';

describe('PreviousSessionFBPage', () => {
  let component: PreviousSessionFBPage;
  let fixture: ComponentFixture<PreviousSessionFBPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviousSessionFBPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviousSessionFBPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
