import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PreviousSessionFBPage } from './previous-session-fb.page';

const routes: Routes = [
  {
    path: '',
    component: PreviousSessionFBPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PreviousSessionFBPage]
})
export class PreviousSessionFBPageModule {}
