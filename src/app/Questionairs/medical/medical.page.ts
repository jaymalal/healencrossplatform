import { Component, OnInit, OnDestroy } from '@angular/core';
import { SurveyJsonManagerService } from '../../shared/Services/survey-json-manager.service';
import * as  Survey from 'survey-angular';
import { NavController } from '@ionic/angular';
import { StorageService } from '../../shared/Services/storage.service';
import { ComplaintInfoService } from '../../shared/Services/complaint-info.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-medical',
  templateUrl: './medical.page.html',
  styleUrls: ['./medical.page.scss'],
})
export class MedicalPage implements OnInit, OnDestroy {

  surveyJsonData: any;
  questionaireId: any;
  patientId: any;
  complaintId: any;
  sessionId: any;
  mainArea: any;

  private pageSubArray: Subscription[] = [];

  public set pageSub$(value: Subscription) {
    this.pageSubArray.push(value);
  }

  constructor(private surveyJsonService: SurveyJsonManagerService,
    private nav: NavController,
    private complaintInfoService: ComplaintInfoService,
    private storageService: StorageService) { }

  ngOnInit() {

    this.storageService.getItem('patientId').then((response: any) => {
      console.log('patientId on medical Page:', response);
      this.patientId = response;
    });

    this.storageService.getItem('complaintId').then((response: any) => {
     // console.log(response);
      this.complaintId = response;
    });

    this.storageService.getItem('sessionId').then((response: any) => {
      console.log('sessionId On medical page:', response);
      this.sessionId = response;
    });

    this.pageSub$ = this.complaintInfoService.getChiefArea.subscribe(res => {
      this.mainArea = res;
      const jsonKey = this.mainArea + '_medicalhistory';
      this.loadSurveyJson(jsonKey);

    });


    this.loadSurveyJson('b_overview');
  }

  private loadSurveyJson(key: string) {
    this.pageSub$ = this.surveyJsonService.getQuestionnaireByName(key).subscribe(res => {
      this.surveyJsonData = res.json;
      this.questionaireId = res.id;
      const survey = new Survey.Model('' + this.surveyJsonData);
      survey.data = {
        self: this,
      };
      survey.onComplete.add(this.sendDataToServerAlt);
      survey.onComplete.add(() => {
        survey.clear();

      });
      survey.onComplete.add(this.sendDataToServer);
      Survey.SurveyNG.render('surveyElement', { model: survey });
    });
  }

  moveForTime() {
    this.nav.navigateForward('duration-time');
  }

  sendDataToServerAlt(sendDataToServerAlt: any) {
    console.log(sendDataToServerAlt);

    sendDataToServerAlt.data.self.sendResponseToServerOnMedicalPage(sendDataToServerAlt.data);
  }

  sendResponseToServerOnMedicalPage(sendDataToServerAlt: any) {
    console.log('sendResponseToServerOnMedicalPage called');
    delete sendDataToServerAlt.self;
    const dto = {
        complaintExerciseId: this.questionaireId,
        complaintId: this.complaintId,
        patientId: this.patientId,
        patientQuestionAnswer: sendDataToServerAlt,
        questionnaireId: this.questionaireId,
        remedyPackageId: 1,
        sessionId: this.sessionId
    };
    this.pageSub$ = this.surveyJsonService.SavePatientAnswer(dto, this.questionaireId).subscribe(response => {
      console.log(response);
      // this.navigateBack();
      this.moveForTime();
    });
  }


  sendDataToServer( surveyAns ) {
    console.log('sendDataToServer called');
    const result = surveyAns.data;
    delete result.self;
    const body = {
      complaintExerciseId: this.questionaireId,
      complaintId:  this.complaintId,
      patientId: this.patientId,
      patientQuestionAnswer: result,
      questionnaireId: this.questionaireId,
      remedyPackageId: 1,
      sessionId: this.sessionId
    };
  }

  ngOnDestroy(): void {
    try {
      this.pageSubArray.forEach(sub => sub.unsubscribe());
    } catch (error) {
      console.log('error while unsubscribing:', error);
    }
  }

}
