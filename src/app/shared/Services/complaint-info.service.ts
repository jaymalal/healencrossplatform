import { Injectable } from '@angular/core';
import { BehaviorSubject, from, Observable } from 'rxjs';
import { Patient } from '../module/Patient';
import { HttpClient } from '@angular/common/http';
import { baseUrl } from '../../shared/Constants/constant';
import { StorageService } from './storage.service';
import { map, catchError } from 'rxjs/operators';
import { ChiefComplaint } from '../module/exercise';
import { ChiefComplaintRes } from '../module/cheifComplaint';
@Injectable({
  providedIn: 'root'
})


export class ComplaintInfoService {
  patientDetails: Patient;
  buyId: any;
  complaintId: any;

  constructor(private  http: HttpClient,
    private storage: StorageService) {
    this.patientDetails = new Patient();
  }
  userRes = new BehaviorSubject<any>({});
  getUserRes = this.userRes.asObservable(); // CI

  chiefArea = new BehaviorSubject<string>('');
  getChiefArea = this.chiefArea.asObservable(); // CI

  mainAreaSide = new BehaviorSubject<number>(0);
  getMainAreaSide = this.mainAreaSide.asObservable(); // CI

  subArea = new BehaviorSubject<any>(null);
  getSubArea = this.subArea.asObservable(); // CI

  subAreaSide = new BehaviorSubject<any>(null);
  getSubAreaSide = this.subAreaSide.asObservable(); // CI

  painIntensity = new BehaviorSubject<number>(0);
  getIntensity = this.painIntensity.asObservable();

  issueType = new BehaviorSubject<any>(0);
  getIssueType = this.issueType.asObservable();

  currentSurveyJson = new BehaviorSubject<string>('');
  getCurrentSurveyJson = this.currentSurveyJson.asObservable();

  setChiefArea(val: string) {
    this.chiefArea.next(val);
  }

  setSide(val: number) {
    this.mainAreaSide.next(val);
  }

  setUserRes(res: any) {
    this.userRes.next(res);
  }

  getBuyId(): number {
    for (const key in this.buyId) {
      if (this.buyId.hasOwnProperty(key)) {
        const element = +key;
        console.log('Element:', element, 'buyId:', this.buyId);
        return element;
      }
    }
  }

  // tslint:disable-next-line: max-line-length
  saveBodyAreaSelections( patientId: number, buyId: number, regionCode: number, areaCode: string, dto: any, primaryComplaint: boolean, issueType: any): Observable<any> {
    if (!patientId) {
      patientId = +this.storage.getItem('patientId');
    }
    console.log('patientId:', patientId);
    regionCode = 1;
    const getBuyId = this.getBuyId();
    // tslint:disable-next-line: max-line-length
    const url =  baseUrl + 'ortho/createComplaint';
    // tslint:disable-next-line: max-line-length
    const mainUrl = url + `/patient/${patientId}/buy/${buyId}/region/${regionCode}/area/${areaCode}/primaryComplaint/${primaryComplaint}/issueType/${issueType}`;
    console.log(mainUrl);
    return this.http.post(mainUrl, dto);
  }

  saveIssueType(patientId: any, buyId: any, issueType: any): Observable<any> {

    const url = baseUrl + `ortho/patient/${patientId}/buy/${buyId}/issue/${issueType}`;
    return this.http.get( url );
  }

  // same API For All Complaints All info will be returned
  getAllComplaintInfoByPatientId( patientId: any ): Observable<any> {
    const url = baseUrl + `complaint/patient/${patientId}`;
    return this.http.get(url).pipe(map ( (res: any) => res.result ));
  }

   // same API For All Complaints Exercise Info will be returned
  getAllChiefComplaintsExerciseInfoByPatientId( patientId: any ): Observable<ChiefComplaint[]> {

    const url = baseUrl + `complaint/patient/${patientId}`;
    const ccData: ChiefComplaint[] = [];
    return this.http.get<ChiefComplaintRes[]>(url).pipe(
      map((res: any) => res.result),
      map((ex: any[]) => {
        for (const i of ex) {

          const temp: ChiefComplaint  = {
            id: i.id,
            weightage: i.weightage || (100 / ex.length),
            exercise: i.complaintExercises
        };

          ccData.push(temp);
        }
        console.log(ccData);
        return ccData;
        })
      );
    }

     getExercisesByChiefCOmplaintId( chiefComplaintId: any ): Observable<any> {

      if ( !chiefComplaintId ) {
        chiefComplaintId = this.storage.getItem('complaintId');
      }
      const url = baseUrl + `complaint/${chiefComplaintId}/exercises`;
      return this.http.get(url).pipe(map((res: any) => res.result));
     }

     getPMExercises(execList: any) {
      const spExercise = [];
      for (const i of execList) {
        if (i.workflowStatus === 'NEW') {

          if (i.exercise.exef === 'PM') {
            spExercise.push({
              code: i.exercise.eqp ? i.exercise.eqp.id || 'NA' : 'none',
              name: i.exercise.eqp ? i.exercise.eqp.name || 'NA' : 'none',
              priority: i.priority || 'NA',
              order: i.orderNr || 'NA',
              mvd: i.exercise.mvd || 'NA',
              mov: i.exercise.mov || 'NA',
              exef: i.exercise.exef,
              exerId: i.id,
              desc: i.exercise.description || 'NA',
              status: i.workflowStatus || 'NA'
            });
          }
        }
      }
      spExercise.sort((a, b) => a.order < b.order ? -1 : a.order < b.order ? 1 : 0);
      this.userRes.next(spExercise);
      return spExercise;
    }

}
