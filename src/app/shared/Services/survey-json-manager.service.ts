import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { baseUrl } from '../../shared/Constants/constant';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class SurveyJsonManagerService {

  constructor(private  http: HttpClient) { }

  pmType = new BehaviorSubject<any>({});
  getPmType = this.pmType.asObservable(); //

  exerciseId = new BehaviorSubject<any>({});
  getExerciseId = this.exerciseId.asObservable(); //

  getQuestionnaireByName(name: string): Observable<any> {

    const url = baseUrl + 'questionaire/name/' + name;
    return this.http.get(url).pipe(map((res: any) => {
      return {
        json: res.result[0].survey_jsondata,
        id: res.result[0].id
       };
    }),
   );
  }

  getQuestionnaireById(id: string): Observable<any> {

    const url = baseUrl + 'questionaire/' + id;
    return this.http.get(url).pipe(map((res: any) => {
      console.log('get Questionnaire By Id Response', res);
      return {
        json: res.result.survey_jsondata,
        id: res.result.id
       };
    }),
   );
  }


  SavePatientAnswer(patientAnswerData: any, id: any): Observable<any> {
    console.log('answerData: ', patientAnswerData);
    const url = baseUrl + `patientAnswer/${id}`;
    console.log('url', url);
    return this.http.post(url, patientAnswerData);
  }

}
