import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { baseUrl } from '../../shared/Constants/constant';
import { ComplaintExercise } from '../module/exercise';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';
import { PatientAnswer } from '../module/questionair';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class SessionManagerService {
  url =  baseUrl + 'session/patient/';

  activePM = 0;
  activeSl = 0;
  isInsertDone = false;
  sessionId: any;
  feedbackState = {
    ceId: '',
    maxSets: 0,
    setsDone: 0,
  };


  constructor(private  http: HttpClient,
    public storageService: StorageService) {

      this.storageService.getItem('sessionId').then((response: any) => {
        console.log('get Item Session Id response:', response);
        this.sessionId = response;
      });
     }

  GetSessionInfoFromId(userId: any) {
     let mainUrl: string;
     mainUrl = this.url + userId ;
     console.log(mainUrl);
     return this.http.get(mainUrl);
  }

  getCurrentSessioNum(sessionId: any): Observable<number> {
    const url = baseUrl + `session/session/${sessionId}`;

    return this.http.get(url).pipe(
      map(
        (session: any) => session.result.sessionSrNo
      )
    );
  }

  createNewSession(patientId: any, buyId: any) {
    let mainUrl: string;
    mainUrl = baseUrl + 'session/patient/' + patientId + '/buy/' + buyId;
    console.log(mainUrl);
    return this.http.post(mainUrl, {});
  }

  closeSession( sessionId: any ) {

    const url = baseUrl + `session/session/${sessionId}`;
    return this.http.put(url, {});
  }

  getBuyIdFromPatientId(patientId: any) {
    let mainUrl: string;
    mainUrl = baseUrl + 'buy/patient/' + patientId;
    console.log(mainUrl);
    return this.http.get(mainUrl);
  }

  createBuyForPatient(patientId: any) {
    let mainUrl: string;
    mainUrl = baseUrl + 'buy/patient/' + patientId + '/package/' + '1';
    console.log(mainUrl);
    return this.http.post(mainUrl, {});
  }

  saveFeedback(userResponse: PatientAnswer, questionairId: number): Observable<any> {

    const url = baseUrl + `patientAnswer/${questionairId}`;
    return this.http.post(url, userResponse);

  }

  getExercisesBySessionId(): Observable<ComplaintExercise[]> {
    let url: any;
    url = baseUrl + `session/session/${this.sessionId}/bucket`;

    return this.http.get(url).pipe(
    map((res: any) => res.result),
  );
  }

  getDoneExercisesBySessionId(): Observable<ComplaintExercise[]> {
    let url: any;
    url = baseUrl + `session/session/${this.sessionId}`;

    return this.http.get(url).pipe(
      map((res: any) => {
        const execList = [];
        const execArr = res.result.exStatuses;
        for (const ex of execArr) {
          execList.push(ex.complaintExercise);
        }
        return execList;
      })
  );
  }


  pauseSession(): Observable<any> {

    const sessionId = this.storageService.getItem('sessionId');
    const patientId = this.storageService.getItem('patientId');

    const url = baseUrl + `session/pause/${sessionId}/patient/${patientId}`;
    return this.http.get(url);
  }

  saveExerciseStatus(userResponse: {complaintExerciseId: string, complaintId: string, patientId: string,
     patientQuestionAnswer: any, remedyPackageId: number, sessionId: string}, execStatus: any): Observable<any> {

    const url = baseUrl + `session/session/${userResponse.sessionId}/chiefcomplaint/${userResponse.complaintId}
    /compliantExercise/${userResponse.complaintExerciseId}`;
    console.log(userResponse, execStatus);

    return this.http.post(url, execStatus);
  }
}
