import { TestBed } from '@angular/core/testing';

import { SignUpManagerService } from './sign-up-manager.service';

describe('SignUpManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SignUpManagerService = TestBed.get(SignUpManagerService);
    expect(service).toBeTruthy();
  });
});
