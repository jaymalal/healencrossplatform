import { TestBed } from '@angular/core/testing';

import { SignInManagerService } from './sign-in-manager.service';

describe('SignInManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SignInManagerService = TestBed.get(SignInManagerService);
    expect(service).toBeTruthy();
  });
});
