import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { baseUrl } from '../../shared/Constants/constant';
import { Patient } from '../module/Patient';
import { encode } from 'punycode';
@Injectable({
  providedIn: 'root'
})
export class SignUpManagerService {

patient: Patient[];
url =  baseUrl + 'register';

constructor(private  http: HttpClient) { }
getSignUpResponse(userData: any) {

  let mainUrl: string;
  mainUrl = this.url;
  console.log(mainUrl);
  console.log('userData :', userData);
  userData['patientStatus'] = 'PATIENT_CREATED';
  const httpOptions = {
    headers: new HttpHeaders({
      'Authorization': 'Basic ' + btoa('devglan-client:devglan-secret')
    })
  };
  return this.http.post(mainUrl, userData, httpOptions);

}

}
