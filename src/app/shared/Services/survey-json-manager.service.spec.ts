import { TestBed } from '@angular/core/testing';

import { SurveyJsonManagerService } from './survey-json-manager.service';

describe('SurveyJsonManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SurveyJsonManagerService = TestBed.get(SurveyJsonManagerService);
    expect(service).toBeTruthy();
  });
});
