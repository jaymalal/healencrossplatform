import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { from } from 'rxjs';
import { promise } from 'protractor';

export interface StorageObj {
  key: any;
}

@Injectable({
  providedIn: 'root'
})


export class StorageService {

  constructor(private storage: Storage) { }

  addItem(key, value) {
    this.storage.set(key, value);
   
  }

  getItem(key): Promise<StorageObj> {
    return this.storage.get(key);

  }

  deleteItem(key) {
   return this.storage.remove(key);
  }

}
