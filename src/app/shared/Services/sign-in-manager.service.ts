import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseUrl } from '../../shared/Constants/constant';
import { Patient } from 'src/app/shared/module/Patient';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { StorageService } from './storage.service';
@Injectable({
  providedIn: 'root'
})
export class SignInManagerService {
  patient: Patient[];
  url =  baseUrl + 'patient/';

  constructor(private  http: HttpClient, private storage: StorageService) { }
  GetLoginResponse(userdata: any) {
    let mainUrl: string;
    mainUrl = this.url + '?' + 'emailId=' + userdata.emailId + '&password=' + userdata.password ;
    console.log(mainUrl);
    return this.http.get(mainUrl);
  }

  getJwtTokenForLogin(userdata: Patient) {
    const url = baseUrl + 'oauth/token?grant_type=password';
    const body: any = new FormData();
    body.append('username', userdata.emailId);
    body.append('password', userdata.password);

    const httpOptions = {
      headers: new HttpHeaders({
        // tslint:disable-next-line: object-literal-key-quotes
        'Authorization': 'Basic ' + btoa('devglan-client:devglan-secret')
      })
    };
    return this.http.post(url, body, httpOptions);
  }
  getPatientByUsername(userName: any): Observable<any> {
    const url = baseUrl + `patient/username/${userName}`;
    return this.http.get(url).pipe(
      map( (res: any) => res.result)
    ) ;
  }
  getUserInfo(token: any) {
    let payload;
    if (token) {
      payload = token.split('.')[1];
      payload = window.atob(payload);
      return JSON.parse(payload);
    } else {
      return null;
    }
  }
}
