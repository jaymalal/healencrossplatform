import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BucketListManagerService {

  constructor() { }

  // current session exec list
  execBucketList = [];
  // save selection of exercises for session
  sessionExecList = [];
  // store all exercise list
  mainBucketList = [];
  deliverExerciesList = [];

  currentExecVal: any;
  currentExec = 0;

  getDeliverExerciesList(time: number, status: string, restictedMovment: boolean, mainBucketList: any) {

    this.deliverExerciesList = [];
    let spExeccList = [];
    const wuExeccList = [];
    const roExeccList = [];
    const jmExeccList = [];
    const flExeccList = [];
    const stExecList = [];
    const pcExecList = [];
    const sgExecList = [];
    const anExecList = [];
    const ndExecList = [];
    const execCount = this.getExecCount(time);

    const exerciseMinCount = execCount.min;

    // step 1: Always add AN (Anchor) exercise to session bucket list
    // P.S. : AN is an exercise which is solution for all problems for perticular area issue.

    // getAnchor exercise form main Bucket list

    for (const key in mainBucketList) {

      if (mainBucketList.hasOwnProperty(key)) {
        const element = mainBucketList[key];

        if (element.workflowStatus === 'NEW' || element.workflowStatus === 'INPROGRESS' || element.exercise.exef === 'AN') {
          if (element.exercise.exef === 'SP') {
            spExeccList.push(element);
          } else if (element.exercise.exef === 'WU') {
            console.log('wu');
            wuExeccList.push(element);
          } else if (element.exercise.exef === 'RO') {
            console.log('ro');
            roExeccList.push(element);
          } else if (element.exercise.exef === 'JM') {
            console.log('jm');
            jmExeccList.push(element);
          } else if (element.exercise.exef === 'FL') {
            console.log('fl');
            flExeccList.push(element);
          } else if (element.exercise.exef === 'AN') {
            console.log('An');
            anExecList.push(element);
          } else if (element.exercise.exef === 'ST') {
            console.log('st');
            stExecList.push(element);
          } else if (element.exercise.exef === 'PC') {
            console.log('pc');
            pcExecList.push(element);
          } else if (element.exercise.exef === 'SG') {
            console.log('sg');
            sgExecList.push(element);
          } else if (element.exercise.exef === 'ND') {
            console.log('nd');
            ndExecList.push(element);
          }
        }
      }
    }

    if (anExecList.length) {
      this.deliverExerciesList.push(anExecList[0]);
    }
    if (spExeccList.length) {

      spExeccList = spExeccList.sort((a, b) => {
        return +a.orderNr < +b.orderNr ? -1 : 1;
      });

      const spCount = this.getSPCountBasedOnNoOfActivities('PHS1');

      for (let index = 0; index < spCount; index++) {
        const element = spExeccList[index];
        this.deliverExerciesList.push(element);

      }
    }
    if (stExecList.length) {

      const stCount = this.getSPCountBasedOnNoOfActivities('PHS1');

      for (let index = 0; index < stCount; index++) {
        try {
          const element = stExecList[index];
          this.deliverExerciesList.push(element);
        } catch (e) {
          break;
        }

      }
    }
    if (pcExecList.length) {

      const pcCount = this.getSPCountBasedOnNoOfActivities('PHS1');

      for (let index = 0; index < pcCount; index++) {
        try {
          const element = pcExecList[index];
          this.deliverExerciesList.push(element);
        } catch (e) {
          break;
        }

      }
    }

    if (wuExeccList.length > 0) {
      const wpCount = this.getWarmUpCount(exerciseMinCount, status);
      for (let index = 0; index < wpCount; index++) {

        try {
          const element = wuExeccList[index];
          this.deliverExerciesList.push(element);
        } catch (error) {

        }

      }

    } else {

      this.getrojmflExec(flExeccList, roExeccList, jmExeccList);
    }

    this.deliverExerciesList = this.deliverExerciesList.filter((value, index, arr) => {
      return value !== [] || value !== undefined;
    });
    if (exerciseMinCount < this.deliverExerciesList.length) {
      this.deliverExerciesList = this.deliverExerciesList.splice(0, exerciseMinCount);
    } else {
      console.log('deliverExercies:', this.deliverExerciesList.length);
      this.getrojmflExec(flExeccList, roExeccList, jmExeccList);
      this.deliverExerciesList = this.deliverExerciesList.splice(0, exerciseMinCount);
      console.log('deliverExerciesAfter:', this.deliverExerciesList.length);
    }

    this.deliverExerciesList = this.deliverExerciesList.filter((val) => {
      return val !== undefined;
    });
    this.deliverExerciesList = [...new Set(this.deliverExerciesList)];
    console.log( this.deliverExerciesList);

    return this.deliverExerciesList;
  }

  getExecCount(time: number): { min: any, max: any } {

    switch (time) {
      case 60:
        return {
          min: 19,
          max: 20
        };
      case 45:

        return {
          min: 16,
          max: 17
        };
      case 30:

        return {
          min: 10,
          max: 10
        };
      case 15:
      default:
        return {
          min: 5,
          max: 5
        };
    }

  }



  getWarmUpCount(execCount: number, status: string) {

    let warmUpCount = 0;
    let per = 10;
    if (status === 'Beginning') {
      per = 10;
    } else if (status === 'Progression') {
      per = 10;
    }

    warmUpCount = execCount * (per / 100);

    return Math.round(warmUpCount);
  }
  getExecCountMinNumber(exec: any) {

    switch (exec) {

      case 'RO':
        return 3;
      case 'JM':
        return 2;
      case 'FL':
        return 3;
    }

  }

  private getrojmflExec(fl_execcList: any[], ro_execcList: any[], jm_execcList: any[]) {
    console.log('hello3');
    const roCount = this.getExecCountMinNumber('RO');
    const jmCount = this.getExecCountMinNumber('JM');
    const flCount = this.getExecCountMinNumber('FL');
    console.log('roCount:', roCount, ' jmcount:', jmCount, ' flcount:', flCount);
    if (fl_execcList.length) {
      for (let index = 0; index < flCount; index++) {
        const element = fl_execcList[index];
        console.log('hello from fl');
        this.deliverExerciesList.push(element);
      }
    }
    if (ro_execcList.length) {
      for (let index = 0; index < roCount; index++) {
        const element = ro_execcList[index];
        console.log('hello from ro');
        this.deliverExerciesList.push(element);
      }
    }
    if (jm_execcList.length) {
      for (let index = 0; index < jmCount; index++) {
        const element = jm_execcList[index];
        console.log('hello from jm');
        this.deliverExerciesList.push(element);
      }
    }
  }

  getSPCountBasedOnNoOfActivities(phase: any) {

    switch (phase) {

      case 'PHS3':
        return 14;

      case 'PHS2':
      case 'PHS1':
      default:
        return 3;
    }

  }

  getNDCountByPhase(phase: any) {
    switch (phase) {

      case 'PHS3':
        return 0;

      case 'PHS2':
      case 'PHS1':
      default:
        return 2;
    }

  }

  getSGCountByPhase(phase: any) {
    switch (phase) {

      case 'PHS3':
        return 1;

      case 'PHS2':
      case 'PHS1':
      default:
        return 2;
    }
  }
}
