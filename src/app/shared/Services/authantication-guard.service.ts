import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { StorageService } from './storage.service';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthanticationGuardService implements CanActivate {
  constructor(private storage: StorageService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any>|Promise<any>|any {
      console.log('guard works');
      this.storage.getItem('signedIn').then(() => true).catch(() => { this.router.navigate(['sign-in']); });
  }
}
