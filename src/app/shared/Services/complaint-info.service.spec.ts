import { TestBed } from '@angular/core/testing';

import { ComplaintInfoService } from './complaint-info.service';

describe('ComplaintInfoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ComplaintInfoService = TestBed.get(ComplaintInfoService);
    expect(service).toBeTruthy();
  });
});
