import { TestBed } from '@angular/core/testing';

import { BucketListManagerService } from './bucket-list-manager.service';

describe('BucketListManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BucketListManagerService = TestBed.get(BucketListManagerService);
    expect(service).toBeTruthy();
  });
});
