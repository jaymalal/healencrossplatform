
export interface Equipment {
    id;
    code;
    name;
  }
export interface JointName {
    code;
    name;
  }
export interface Exercise {
    id;
    title;
    description;
    sd;
    eqp: Equipment;
    code;
    oc;
    so;
    repMin;
    repMax;
    setMin;
    setMax;
    exef;
    pos;
    mov;
    mvd;
    toe;
    holdMin;
    holdMax;
    sessionStart;
    lj: JointName;
  }

export interface ComplaintExercise {
    id;
    ccId;
    exercise: Exercise;
    weightage;
    insertExercise;
    workflowStatus;
    progressionReps;
    progressionSets;
    progressionHold;
    ocCounter;
    soCounter;
    soPainCounter;
    priority?;
  }
export interface CurrentProgression {
    progressionReps;
    progressionSets;
    progressionHold;
  }


export interface ExerciseFeedback {

      complaintExerciseId;
      complaintId;
      patientId;
      patientQuestionAnswer;
      remedyPackageId;
      sessionId;

  }
export interface ChiefComplaint {
    id;
    weightage;
    exercise: ComplaintExercise[];
    phase?;
  }
