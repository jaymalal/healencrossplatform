export interface SubArea {

    pic: number;
    side: string;
    subArea: number;

  }
export interface MainAreaSide {

    pic: number;
    side: string;

  }
export interface ChiefComplaintDto {

    mainArea: string;
    mainPic: number;
    sidepic: MainAreaSide[];
    subAreas: SubArea[];
    userSelections: string[];

  }

export interface ChiefComplaintRes {
    complaintNo;
    patientId;
    buyId;
    regionCode;
    areaCode;
    painIntensity;
    chiefComplaintDto: ChiefComplaintDto;
    bfnp;
    primaryComplaint: boolean;
    issueTypeEnum;
  }
