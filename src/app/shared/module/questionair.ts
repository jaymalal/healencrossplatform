export interface PatientAnswer {
    complaintExerciseId?;
    complaintId?;
    patientId;
    patientQuestionAnswer;
    remedyPackageId?;
    sessionId?;
  }
export interface Question {
    name;
    type;
    title;
    isRequired;
    choices?;
  }

