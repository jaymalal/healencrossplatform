export class Patient {
    id = 0;
    firstName = '';
    middleName = '';
    lastName = '';
    dateOfBirth = '';
    gender = '';
    weight = '';
    height = '';
    addLine1 = '';
    addLine2 = '';
    city = '';
    state = '';
    zip = 0;
    mobileNumber = 0;
    emailId = '';
    password = '';
    emailVerified = true;
    mobileVerified = true;
}
