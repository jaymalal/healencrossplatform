import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VideoDisplayRoutingModule } from './video-displayRouting.module';

import { CommonVideoDisplayPage } from './common-video-display.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VideoDisplayRoutingModule
  ],
  declarations: [CommonVideoDisplayPage],
  entryComponents: [CommonVideoDisplayPage]
})
export class CommonVideoDisplayPageModule {}
