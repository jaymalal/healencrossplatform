import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonVideoDisplayPage } from './common-video-display.page';

describe('CommonVideoDisplayPage', () => {
  let component: CommonVideoDisplayPage;
  let fixture: ComponentFixture<CommonVideoDisplayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonVideoDisplayPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonVideoDisplayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
