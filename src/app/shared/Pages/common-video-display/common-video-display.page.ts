import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-common-video-display',
  templateUrl: './common-video-display.page.html',
  styleUrls: ['./common-video-display.page.scss'],
})
export class CommonVideoDisplayPage implements OnInit {

  constructor( private modal: ModalController) { }

  src = 'https://aktive-health.s3.ap-south-1.amazonaws.com/assets/session/901/1276/1276_1avr';

  @ViewChild('video', { static: true }) videoTag: ElementRef;
  video: HTMLVideoElement;
  videoSub$: Subscription;

  ngOnInit() {

    this.src = 'https://aktive-health.s3.ap-south-1.amazonaws.com/assets/videos/0004IVR.mp4';

  }

  closeModal() {
    console.log(this.modal);
    this.modal.dismiss().then(res => {
      console.log('model closed');
    });

  }
}
