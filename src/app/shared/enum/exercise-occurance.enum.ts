export enum ExerciseOccurrence {
  OC1 = 1,
  OC2 = 2,
  OC3 = 3,
  OC4 = 4,
  OC5 = 5,
  OC6 = 6,
  OC7 = 7
}
