export enum ExerciseFeedback {
  strenuous = 'strenuous',
  rangeRestricted = 'My range was restricted',
  tingling = 'I felt tingling/numbness sensation',
  noise = 'My joint was making noises while I performed the exercise',
  notFollow = 'I couldn’t follow the instructions',
  speedFast = 'Exercise speed was too fast',
  speedSlow = 'Exercise speed was too slow',
  afraidPain = 'I have done similar exercises, which had caused pain during or after the exercise',
  notConfident = 'I don\'t feel confident to do this without supervision',
  usualPain = 'my usual pain increased',
  newPain = 'Pain felt in another area',
}
// Replace values with actual values in database
export enum Feedback{
  isPositive = '565',

}
