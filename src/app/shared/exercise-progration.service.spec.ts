import { TestBed } from '@angular/core/testing';

import { ExerciseProgrationService } from './exercise-progration.service';

describe('ExerciseProgrationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExerciseProgrationService = TestBed.get(ExerciseProgrationService);
    expect(service).toBeTruthy();
  });
});
