import { Injectable } from '@angular/core';
import { ComplaintExercise, CurrentProgression } from '../shared/module/exercise';

@Injectable({
  providedIn: 'root'
})
export class ExerciseProgrationService {

  constructor() { }

  getExerciseProgression(exercise: ComplaintExercise): CurrentProgression {
    const so: CurrentProgression = {
      progressionReps: 1,
      progressionSets: 1,
      progressionHold: 1
    };

    if (exercise.exercise.toe === 'R') {

      if (exercise.workflowStatus === 'NEW') {
        so.progressionReps = exercise.exercise.repMin;
        so.progressionSets = exercise.exercise.setMin;

      } else {

        so.progressionReps = exercise.progressionReps;
        so.progressionSets = exercise.progressionSets;

      }

    } else {

      if (exercise.workflowStatus === 'NEW') {
        so.progressionHold = exercise.exercise.holdMin;
        so.progressionSets = exercise.exercise.setMin;

      } else {
        so.progressionHold = exercise.progressionHold;
        so.progressionSets = exercise.progressionSets;

      }

    }

    return so;
  }

  processSoProgression(exercise: any, feedback: any) {


    let reps = exercise.progressionReps + 6;
    if (reps > exercise.exercise.repMax) {
      reps = exercise.exercise.repMax;
    }

  }

  getSOProgressionofExec(exercise: ComplaintExercise): { sets: any, reps: any, soCounter: any, soPainCounter: any, hold: any } {


    if (exercise.exercise.toe == 'H') {

      switch (exercise.exercise.so) {

        case 1:
          return this.getHoldByStateSO1(exercise.soCounter, exercise);
        case 1.5:
          return this.getHoldByStateSO15(exercise.soCounter, exercise);
        case 2:
          return this.getHoldByStateSO2(exercise.soCounter, exercise);
        default:
          return this.getHoldByStateSO5(exercise.soCounter, exercise);

      }
    } else {

      switch (exercise.exercise.so) {

        case 1:
          return this.getSetAndRepsByStateSO1(exercise.soCounter, exercise);
        case 1.5:
          return this.getSetAndRepsByStateSO15(exercise.soCounter, exercise);
        case 2:
          return this.getSetAndRepsByStateSO2(exercise.soCounter, exercise);
        default:
          return this.getSetAndRepsByStateSO5(exercise.soCounter, exercise);
      }
    }
  }

  getSetAndRepsByStateSO1(counter: any, exec: any): { sets: any, reps: any, soCounter: any, soPainCounter: any, hold: any } {

    const res = {
      sets: 0,
      reps: 0,
      soCounter: 0,
      soPainCounter: 0,
      hold: 0
    };
    switch (counter) {

      case 0: {
        let currentReps = exec.progressionReps;
        if (currentReps < exec.exercise.repMin) {
          currentReps = exec.exercise.repMin;
        }
        const maxReps = exec.exercise.repMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentReps + 6) >= maxReps) {
          res.reps = maxReps;
          res.sets = exec.exercise.setMin;
          res.soCounter = 1;
          res.soPainCounter = 0;
          return res;
        } else {
          res.reps = currentReps + 6;
          res.sets = exec.exercise.setMin;
          res.soPainCounter = 0;
          res.soCounter = 0;
          return res;
        }
      }

      case 1: {
        const minReps = exec.exercise.repMin;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        res.reps = minReps;
        if ((exec.progressionSets + 1) <= exec.exercise.setMax) {
          res.sets = exec.progressionSets + 1;
          res.soCounter = 0;
        } else {
          res.sets = exec.exercise.setMax;
          res.soCounter = 2;
        }
        res.soPainCounter = 0;
        return res;
      }
      case 2: {
        const currentReps = exec.progressionReps;
        const maxReps = exec.exercise.repMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentReps + 6) >= maxReps) {
          res.reps = maxReps;
          res.sets = exec.exercise.setMax;
          res.soCounter = 6;
          res.soPainCounter = 0;
          return res;
        } else {
          res.reps = currentReps + 6;
          res.sets = exec.exercise.setMax;
          res.soPainCounter = 0;
          res.soCounter = 2;
          return res;
        }
      }
      case 3: {
        const maxReps = exec.exercise.repMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        res.reps = maxReps;
        res.sets = exec.exercise.setMax;
        res.soPainCounter = 0;
        res.soCounter = 4;
        return res;
      }

    }
  }

  getSetAndRepsByStateSO15(counter: any, exec: any): { sets: any, reps: any, soCounter: any, soPainCounter: any, hold: any } {

    const res = {
      sets: 0,
      reps: 0,
      soCounter: 0,
      soPainCounter: 0,
      hold: 0
    };
    switch (counter) {

      case 0: {
        let currentReps = exec.progressionReps;
        const maxReps = exec.exercise.repMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if (currentReps < exec.exercise.repMin) {
          currentReps = exec.exercise.repMin;
        }
        if ((currentReps + 6) >= maxReps) {
          res.reps = maxReps;
          res.sets = exec.exercise.setMin;
          res.soCounter = 1;
          res.soPainCounter = 0;
          return res;
        } else {
          res.reps = currentReps + 6;
          res.sets = exec.exercise.setMin;
          res.soPainCounter = 0;
          res.soCounter = 0;
          return res;
        }
      }
      case 1: {
        const currentReps = exec.progressionReps;
        const maxReps = exec.exercise.repMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentReps + 6) >= maxReps) {
          res.reps = maxReps;
          res.sets = exec.exercise.setMin;
          res.soCounter = 2;
          res.soPainCounter = 0;
          return res;
        } else {
          res.reps = currentReps + 6;
          res.sets = exec.exercise.setMin;
          res.soPainCounter = 0;
          res.soCounter = 1;
          return res;
        }
      }
      case 2: {
        const currentReps = exec.progressionReps;
        const maxReps = exec.exercise.repMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentReps + 6) >= maxReps) {
          res.reps = maxReps;
          res.sets = exec.exercise.setMin;
          res.soCounter = 3;
          res.soPainCounter = 0;
          return res;
        } else {
          res.reps = currentReps + 6;
          res.sets = exec.exercise.setMin;
          res.soPainCounter = 0;
          res.soCounter = 2;
          return res;
        }
      }
      //
      case 3: {
        const minReps = exec.exercise.repMin;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        res.reps = minReps;
        if ((exec.progressionSets + 1) <= exec.exercise.setMax) {
          res.sets = exec.progressionSets + 1;
          res.soCounter = 3;
        } else {
          res.sets = exec.exercise.setMax;
          res.soCounter = 4;
        }
        res.soPainCounter = 0;
        return res;
      }
      case 4: {
        const minReps = exec.exercise.repMin;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        res.reps = minReps;
        if ((exec.progressionSets + 1) <= exec.exercise.setMax) {
          res.sets = exec.progressionSets + 1;
          res.soCounter = 4;
        } else {
          res.sets = exec.exercise.setMax;
          res.soCounter = 5;
        }
        res.soPainCounter = 0;
        return res;
      }
      case 5: {
        const minReps = exec.exercise.repMin;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        res.reps = minReps;
        if ((exec.progressionSets + 1) <= exec.exercise.setMax) {
          res.sets = exec.progressionSets + 1;
          res.soCounter = 5;
        } else {
          res.sets = exec.exercise.setMax;
          res.soCounter = 6;
        }
        res.soPainCounter = 0;
        return res;
      }
      //
      case 6: {
        const currentReps = exec.progressionReps;
        const maxReps = exec.exercise.repMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentReps + 6) >= maxReps) {
          res.reps = maxReps;
          res.sets = exec.exercise.setMax;
          res.soCounter = 7;
          res.soPainCounter = 0;
          return res;
        } else {
          res.reps = currentReps + 6;
          res.sets = exec.exercise.setMax;
          res.soPainCounter = 0;
          res.soCounter = 6;
          return res;
        }
      }
      case 7: {
        const currentReps = exec.progressionReps;
        const maxReps = exec.exercise.repMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentReps + 6) >= maxReps) {
          res.reps = maxReps;
          res.sets = exec.exercise.setMax;
          res.soCounter = 8;
          res.soPainCounter = 0;
          return res;
        } else {
          res.reps = currentReps + 6;
          res.sets = exec.exercise.setMax;
          res.soPainCounter = 0;
          res.soCounter = 7;
          return res;
        }
      }

    }
  }
  getSetAndRepsByStateSO2(counter: any, exec: any): { sets: any, reps: any, soCounter: any, soPainCounter: any, hold: any } {

    const res = {
      sets: 0,
      reps: 0,
      soCounter: 0,
      soPainCounter: 0,
      hold: 0
    };
    switch (counter) {

      case 0: {
        let currentReps = exec.progressionReps;
        const maxReps = exec.exercise.repMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if (currentReps < exec.exercise.repMin) {
          currentReps = exec.exercise.repMin;
        }
        if ((currentReps + 6) >= maxReps) {
          res.reps = maxReps;
          res.sets = exec.exercise.setMin;
          res.soCounter = 1;
          res.soPainCounter = 0;
          return res;
        } else {
          res.reps = currentReps + 6;
          res.sets = exec.exercise.setMin;
          res.soPainCounter = 0;
          res.soCounter = 0;
          return res;
        }
      }
      case 1: {
        const currentReps = exec.progressionReps;
        const maxReps = exec.exercise.repMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentReps + 6) >= maxReps) {
          res.reps = maxReps;
          res.sets = exec.exercise.setMin;
          res.soCounter = 2;
          res.soPainCounter = 0;
          return res;
        } else {
          res.reps = currentReps + 6;
          res.sets = exec.exercise.setMin;
          res.soPainCounter = 0;
          res.soCounter = 1;
          return res;
        }
      }
      //
      case 2: {
        const minReps = exec.exercise.repMin;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        res.reps = minReps;
        if ((exec.progressionSets + 1) <= exec.exercise.setMax) {
          res.sets = exec.progressionSets + 1;
          res.soCounter = 2;
        } else {
          res.sets = exec.exercise.setMax;
          res.soCounter = 3;
        }
        res.soPainCounter = 0;
        return res;
      }
      case 3: {
        const minReps = exec.exercise.repMin;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        res.reps = minReps;
        if ((exec.progressionSets + 1) <= exec.exercise.setMax) {
          res.sets = exec.progressionSets + 1;
          res.soCounter = 3;
        } else {
          res.sets = exec.exercise.setMax;
          res.soCounter = 4;
        }
        res.soPainCounter = 0;
        return res;
      }
      //
      case 4: {
        const currentReps = exec.progressionReps;
        const maxReps = exec.exercise.repMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentReps + 6) >= maxReps) {
          res.reps = maxReps;
          res.sets = exec.exercise.setMax;
          res.soCounter = 5;
          res.soPainCounter = 0;
          return res;
        } else {
          res.reps = currentReps + 6;
          res.sets = exec.exercise.setMax;
          res.soPainCounter = 0;
          res.soCounter = 4;
          return res;
        }
      }
      case 5: {
        const currentReps = exec.progressionReps;
        const maxReps = exec.exercise.repMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentReps + 6) >= maxReps) {
          res.reps = maxReps;
          res.sets = exec.exercise.setMax;
          res.soCounter = 6;
          res.soPainCounter = 0;
          return res;
        } else {
          res.reps = currentReps + 6;
          res.sets = exec.exercise.setMax;
          res.soPainCounter = 0;
          res.soCounter = 5;
          return res;
        }
      }

      case 6: {
        const maxReps = exec.exercise.repMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        res.reps = maxReps;
        res.sets = exec.exercise.setMax;
        res.soPainCounter = 0;
        res.soCounter = 7;
        return res;
      }

    }
  }
  getSetAndRepsByStateSO5(counter: any, exec: any): { sets: any, reps: any, soCounter: any, soPainCounter: any, hold: any } {
    const res = {
      sets: 0,
      reps: 0,
      soCounter: 0,
      soPainCounter: 0,
      hold: 0
    };
    res.sets = exec.exercise.setMax;
    res.reps = exec.exercise.repMax;
    res.soCounter = 1;
    res.soPainCounter = 0;
    return res;
  }

  //  Hold exercise progression
  // process hold exercise progression

  getHoldByStateSO1(counter: any, exec: ComplaintExercise): { sets: any, reps: any, soCounter: any, soPainCounter: any, hold: any } {

    const res = {
      sets: 0,
      reps: 0,
      soCounter: 0,
      soPainCounter: 0,
      hold: 0
    };
    switch (counter) {

      case 0: {
        let currentHold = exec.progressionHold;
        if (currentHold < exec.exercise.holdMin) {
          currentHold = exec.exercise.holdMin;
        }
        const maxHold = exec.exercise.holdMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentHold + 10) >= maxHold) {
          res.hold = maxHold;
          res.sets = exec.progressionSets || exec.exercise.setMin;
          res.soCounter = 1;
          res.soPainCounter = 0;
          return res;
        } else {
          res.hold = currentHold + 10;
          res.sets = exec.progressionSets || exec.exercise.setMin;
          res.soPainCounter = 0;
          res.soCounter = 0;
          return res;
        }
      }

      case 1: {
        const minHold = exec.exercise.holdMin;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        res.hold = minHold;
        if ((exec.progressionSets + 1) <= exec.exercise.setMax) {
          res.sets = exec.progressionSets + 1;
          res.soCounter = 0;
        } else {
          res.sets = exec.exercise.setMax;
          res.soCounter = 2;
        }
        res.soPainCounter = 0;
        return res;
      }
      case 2: {
        const currentHold = exec.progressionHold;
        const maxHold = exec.exercise.holdMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentHold + 10) >= maxHold) {
          res.hold = maxHold;
          res.sets = exec.exercise.setMax;
          res.soCounter = 2;
          res.soPainCounter = 0;
          return res;
        } else {
          res.hold = currentHold + 10;
          res.sets = exec.exercise.setMax;
          res.soPainCounter = 0;
          res.soCounter = 2;
          return res;
        }
      }
      case 3: {
        const maxHold = exec.exercise.holdMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        res.hold = maxHold;
        res.sets = exec.exercise.setMax;
        res.soPainCounter = 0;
        res.soCounter = 4;
        return res;
      }

      default: {
        res.hold = exec.exercise.holdMax;
        res.sets = exec.exercise.setMax;
        res.reps = exec.exercise.repMax;
        return res;
      }

    }
  }

  getHoldByStateSO15(counter: any, exec: ComplaintExercise): { sets: any, reps: any, soCounter: any, soPainCounter: any, hold: any } {

    const res = {
      sets: 0,
      reps: 0,
      soCounter: 0,
      soPainCounter: 0,
      hold: 0
    };
    switch (counter) {

      case 0: {
        let currentHold = exec.progressionHold;
        const maxHold = exec.exercise.holdMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if (currentHold < exec.exercise.holdMin) {
          currentHold = exec.exercise.holdMin;
        }
        if ((currentHold + 10) >= maxHold) {
          res.hold = maxHold;
          res.sets = exec.exercise.setMin;
          res.soCounter = 1;
          res.soPainCounter = 0;
          return res;
        } else {
          res.hold = currentHold + 10;
          res.sets = exec.exercise.setMin;
          res.soPainCounter = 0;
          res.soCounter = 0;
          return res;
        }
      }
      case 1: {
        const currentHold = exec.progressionHold;
        const maxHold = exec.exercise.holdMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentHold + 10) >= maxHold) {
          res.hold = maxHold;
          res.sets = exec.exercise.setMin;
          res.soCounter = 2;
          res.soPainCounter = 0;
          return res;
        } else {
          res.hold = currentHold + 10;
          res.sets = exec.exercise.setMin;
          res.soPainCounter = 0;
          res.soCounter = 1;
          return res;
        }
      }
      case 2: {
        const currentHold = exec.progressionHold;
        const maxHold = exec.exercise.holdMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentHold + 10) >= maxHold) {
          res.hold = maxHold;
          res.sets = exec.exercise.setMin;
          res.soCounter = 3;
          res.soPainCounter = 0;
          return res;
        } else {
          res.hold = currentHold + 10;
          res.sets = exec.exercise.setMin;
          res.soPainCounter = 0;
          res.soCounter = 2;
          return res;
        }
      }
      //
      case 3: {
        const minHold = exec.exercise.holdMin;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        res.reps = minHold;
        if ((exec.progressionSets + 1) <= exec.exercise.setMax) {
          res.sets = exec.progressionSets + 1;
          res.soCounter = 3;
        } else {
          res.sets = exec.exercise.setMax;
          res.soCounter = 4;
        }
        res.soPainCounter = 0;
        return res;
      }
      case 4: {
        const minHold = exec.exercise.holdMin;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        res.hold = minHold;
        if ((exec.progressionSets + 1) <= exec.exercise.setMax) {
          res.sets = exec.progressionSets + 1;
          res.soCounter = 4;
        } else {
          res.sets = exec.exercise.setMax;
          res.soCounter = 5;
        }
        res.soPainCounter = 0;
        return res;
      }
      case 5: {
        const minHold = exec.exercise.holdMin;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        res.hold = minHold;
        if ((exec.progressionSets + 1) <= exec.exercise.setMax) {
          res.sets = exec.progressionSets + 1;
          res.soCounter = 5;
        } else {
          res.sets = exec.exercise.setMax;
          res.soCounter = 6;
        }
        res.soPainCounter = 0;
        return res;
      }
      //
      case 6: {
        const currentHold = exec.progressionHold;
        const maxHold = exec.exercise.holdMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentHold + 10) >= maxHold) {
          res.hold = maxHold;
          res.sets = exec.exercise.setMax;
          res.soCounter = 7;
          res.soPainCounter = 0;
          return res;
        } else {
          res.hold = currentHold + 10;
          res.sets = exec.exercise.setMax;
          res.soPainCounter = 0;
          res.soCounter = 6;
          return res;
        }
      }
      case 7: {
        const currentHold = exec.progressionHold;
        const maxHold = exec.exercise.holdMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentHold + 10) >= maxHold) {
          res.hold = maxHold;
          res.sets = exec.exercise.setMax;
          res.soCounter = 8;
          res.soPainCounter = 0;
          return res;
        } else {
          res.hold = currentHold + 10;
          res.sets = exec.exercise.setMax;
          res.soPainCounter = 0;
          res.soCounter = 7;
          return res;
        }
      }

    }
  }
  getHoldByStateSO2(counter: any, exec: ComplaintExercise): { sets: any, reps: any, soCounter: any, soPainCounter: any, hold: any } {

    const res = {
      sets: 0,
      reps: 0,
      soCounter: 0,
      soPainCounter: 0,
      hold: 0
    };
    switch (counter) {

      case 0: {
        let currentHold = exec.progressionHold;
        const maxHold = exec.exercise.holdMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if (currentHold < exec.exercise.holdMin) {
          currentHold = exec.exercise.holdMin;
        }
        if ((currentHold + 10) >= maxHold) {
          res.hold = maxHold;
          res.sets = exec.exercise.setMin;
          res.soCounter = 1;
          res.soPainCounter = 0;
          return res;
        } else {
          res.hold = currentHold + 10;
          res.sets = exec.exercise.setMin;
          res.soPainCounter = 0;
          res.soCounter = 0;
          return res;
        }
      }
      case 1: {
        const currentHold = exec.progressionHold;
        const maxHold = exec.exercise.holdMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentHold + 10) >= maxHold) {
          res.hold = maxHold;
          res.sets = exec.exercise.setMin;
          res.soCounter = 2;
          res.soPainCounter = 0;
          return res;
        } else {
          res.hold = currentHold + 10;
          res.sets = exec.exercise.setMin;
          res.soPainCounter = 0;
          res.soCounter = 1;
          return res;
        }
      }
      //
      case 2: {
        const minHold = exec.exercise.holdMin;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        res.hold = minHold;
        if ((exec.progressionSets + 1) <= exec.exercise.setMax) {
          res.sets = exec.progressionSets + 1;
          res.soCounter = 2;
        } else {
          res.sets = exec.exercise.setMax;
          res.soCounter = 3;
        }
        res.soPainCounter = 0;
        return res;
      }
      case 3: {
        const minHold = exec.exercise.holdMin;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        res.hold = minHold;
        if ((exec.progressionSets + 1) <= exec.exercise.setMax) {
          res.sets = exec.progressionSets + 1;
          res.soCounter = 3;
        } else {
          res.sets = exec.exercise.setMax;
          res.soCounter = 4;
        }
        res.soPainCounter = 0;
        return res;
      }
      //
      case 4: {
        const currentHold = exec.progressionHold;
        const maxHold = exec.exercise.holdMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentHold + 10) >= maxHold) {
          res.hold = maxHold;
          res.sets = exec.exercise.setMax;
          res.soCounter = 5;
          res.soPainCounter = 0;
          return res;
        } else {
          res.hold = currentHold + 10;
          res.sets = exec.exercise.setMax;
          res.soPainCounter = 0;
          res.soCounter = 4;
          return res;
        }
      }
      case 5: {
        const currentHold = exec.progressionHold;
        const maxHold = exec.exercise.holdMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        if ((currentHold + 10) >= maxHold) {
          res.hold = maxHold;
          res.sets = exec.exercise.setMax;
          res.soCounter = 6;
          res.soPainCounter = 0;
          return res;
        } else {
          res.hold = currentHold + 10;
          res.sets = exec.exercise.setMax;
          res.soPainCounter = 0;
          res.soCounter = 5;
          return res;
        }
      }

      case 6: {
        const maxHold = exec.exercise.holdMax;
        res.soCounter = exec.soCounter;
        res.soPainCounter = exec.soPainCounter;
        res.hold = maxHold;
        res.sets = exec.exercise.setMax;
        res.soPainCounter = 0;
        res.soCounter = 7;
        return res;
      }

    }
  }
  getHoldByStateSO5(counter: any, exec: ComplaintExercise): { sets: any, reps: any, soCounter: any, soPainCounter: any, hold: any } {
    const res = {
      sets: 0,
      reps: 0,
      soCounter: 0,
      soPainCounter: 0,
      hold: 0
    };
    res.sets = exec.exercise.setMax;
    res.reps = exec.exercise.repMax;
    res.hold = exec.exercise.holdMax;
    res.soCounter = 1;
    res.soPainCounter = 0;
    return res;
  }

}
