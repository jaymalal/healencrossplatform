import { Component, OnInit, OnDestroy } from '@angular/core';
// import { from } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { StorageService } from 'src/app/shared/Services/storage.service';
import { Validators } from '@angular/forms';
import { Patient } from 'src/app/shared/module/Patient';
import { SignUpManagerService } from '../../shared/Services/sign-up-manager.service';
import { AlertController, NavController } from '@ionic/angular';
import { SessionManagerService } from '../../shared/Services/session-manager.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss']
})
export class SignUpPage implements OnInit, OnDestroy {

  // Variables
  patientObj: Patient;
  isSubmitted = false;
  private pageSubArray: Subscription[] = [];

  public set pageSub$(value: Subscription) {
    this.pageSubArray.push(value);
  }

  profileForm = this.fb.group({
    firstName: ['', Validators.required],
    middleName: [''],
    lastName: ['', Validators.required],
    password :  ['', Validators.required] ,
    dateOfBirth :  [''],
    mobileNumber : ['', Validators.required],
    weight :  [''],
    gender :  [''],
    height :  [''] ,
    addLine1 :  [''] ,
    addLine2 :  [''] ,
    city: [''],
    state: [''],
    zip: [''],
    username: ['', Validators.required],
    emailVerified: [true],
    mobileVerified: [true],
    id: [25],
    patientStatus: 'PATIENT_CREATED',
  });

  constructor(
    private fb: FormBuilder,
    private alertCtrl: AlertController,
    private signUpManagerService: SignUpManagerService,
    private storage: StorageService,
   public navCtrl: NavController,
   private sessionManager: SessionManagerService) {
    this.patientObj = new Patient();
  }

  ngOnInit() {
    this.storage.deleteItem('patientId');
  }

  backToSignIn() {
    this.navCtrl.back();
  }

  moveToSignIn() {
    this.navCtrl.navigateRoot(['sign-in']);
  }

  getcontrol(): any {
    return this.profileForm.controls;
  }

  SubmitSignUpInfo(confirmPassword) {
    this.isSubmitted = true;
    // TODO: Use EventEmitter with form value
    console.warn(this.profileForm);
    const formValue = this.profileForm.value;
    const isValidData: boolean = this.validateForm(confirmPassword);
    if (isValidData) {
      this.pageSub$ = this.signUpManagerService.getSignUpResponse(formValue).subscribe((signUpResult: any) => {
          console.log('from app signUp page ' + JSON.stringify(signUpResult));
          // if (signUpResult.status === 200) {
          this.patientObj = signUpResult.result;
          if ( this.patientObj.id > 0 ) {
            this.alertCtrl.create({
                header: 'Alert',
                // tslint:disable-next-line: max-line-length
                message: 'you have successfully created account. Please Sign-In Now.',
                buttons: [
                  {
                    text: 'Ok',
                    role: 'Ok'
                  }]
            }).then(alertEl => {
              alertEl.present();
              this.moveToSignIn();
            });
            } else {
              this.alertCtrl.create({
                header: 'Alert',
                message: 'Something went wrong! Please try again.2',
                buttons: [
                  {
                    text: 'Ok',
                    role: 'Ok'
                  }]
            }).then(alertEl => {
              alertEl.present();
            });
            }
          // } else {
          //   this.alertCtrl.create({
          //     header: 'Alert',
          //     message: 'Something went wrong! please try again.',
          //     buttons: [
          //       {
          //         text: 'Ok',
          //         role: 'Ok'
          //       }]
          // }).then(alertEl => {
          //   alertEl.present();
          // });
          // }
        }, (error) => {
          this.alertCtrl.create({
            header: 'Alert',
            message: 'Something went wrong! Please try again.1',
            buttons: [
              {
                text: 'Ok',
                role: 'Ok'
              }]
        }).then(alertEl => {
          alertEl.present();
        });
        });
    } else {
         return;
    }

  }

validateForm(confirmPassword: any): boolean {
    console.log(confirmPassword);
    if (this.profileForm.status === 'VALID') {
     if (this.profileForm.value.password === confirmPassword) {
        return true;
       } else {
        this.alertCtrl.create({
          header: 'Alert',
          message: 'Password And Confirm password Does Not Match',
          buttons: [
            {
              text: 'Ok',
              role: 'Ok'
            }]
      }).then(alertEl => {
        alertEl.present();
      });
        return false;
      }
    } else {
      this.alertCtrl.create({
        header: 'Alert',
        message: 'Please Enter All Fields!',
        buttons: [
          {
            text: 'Ok',
            role: 'Ok'
          }]
    }).then(alertEl => {
      alertEl.present();
    });
      return false;
    }
  }
  ngOnDestroy(): void {
    try {
      this.pageSubArray.forEach(sub => sub.unsubscribe());

    } catch (error) {
      console.log('error while unsubscribing:', error);
    }
  }
}

