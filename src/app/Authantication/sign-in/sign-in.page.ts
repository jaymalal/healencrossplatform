import { Component, OnInit, OnDestroy } from '@angular/core';
import { SignInManagerService } from 'src/app/shared/Services/sign-in-manager.service';
import { Patient } from 'src/app/shared/module/Patient';
import { AlertController, NavController } from '@ionic/angular';
import { FormBuilder } from '@angular/forms';
import { StorageService } from 'src/app/shared/Services/storage.service';
import { Validators } from '@angular/forms';
import { SessionManagerService } from '../../shared/Services/session-manager.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.page.html',
  styleUrls: ['./sign-in.page.scss'],
})
export class SignInPage implements OnInit, OnDestroy {

  // Variables
 patientObj: Patient;
 public formsModuleObj = '';
 // Array
  buyArray = [];

  private pageSubArray: Subscription[] = [];

  public set pageSub$(value: Subscription) {
    this.pageSubArray.push(value);
  }

  constructor(private signInManagerService: SignInManagerService,
    private storage: StorageService,
    private alertCtrl: AlertController,
    public navCtrl: NavController,
    private formBuilder: FormBuilder,
    private sessionManager: SessionManagerService) {
     this.patientObj = new Patient();
   }

   signInForm = this.formBuilder.group({
    emailId: ['', Validators.required],
    password :  ['', Validators.required] ,
  });




setUpAfterSignUp() {
  // store all values in data base for future use
  this.storage.deleteItem('signedIn');
  this.storage.addItem('signedIn', true);
  this.storage.addItem('patientId', this.patientObj.id);
  this.storage.addItem('emailId', this.patientObj.emailId);
  this.storage.addItem('password', this.patientObj.password);
  this.storage.addItem('firstName', this.patientObj.firstName);
  this.storage.addItem('lastName', this.patientObj.lastName);
  this.storage.addItem('dateOfBirth', this.patientObj.dateOfBirth);
  this.storage.addItem('weight', this.patientObj.weight);
  this.storage.addItem('height', this.patientObj.height);
  this.storage.addItem('gender', this.patientObj.gender);
  this.storage.addItem('mobileNumber', this.patientObj.mobileNumber);
  console.log('mobile number while saving:', this.patientObj.mobileNumber);
}
  ngOnInit() {

    this.storage.deleteItem('patientId');
  }

  getBuyId(patientId: any) {
    this.pageSub$ = this.sessionManager.getBuyIdFromPatientId(patientId).subscribe((getBuyResult: any) => {
      console.log(JSON.stringify(getBuyResult));
      const tempArray = getBuyResult.result;
      const keys = Object.keys(tempArray);
      console.log(keys);
      if (keys.length > 0 || getBuyResult.result.length > 0) {
        // this.buyArray = keys;
        console.log('buyId : ', keys[0]);
        this.storage.addItem('buyId', keys[0]);
      } else {
          this.createBuyForPatient(patientId);
       }
      }), (error)=>{
        console.log('error for getting buy id');
        
      };
  }

  createBuyForPatient(patientId: any) {
    this.pageSub$ = this.sessionManager.createBuyForPatient(patientId).subscribe((createBuyResponse: any) => {
      console.log(JSON.stringify(createBuyResponse));
      console.log('recived buyId from createBuyIdApi', createBuyResponse.result.id);
     // this.buyArray.push(createBuyResponse.result.id);
      this.storage.addItem('buyId', createBuyResponse.result.id);
    });
  }


  signInButtonClicked() {

    const formValue = this.signInForm.value;
    if (formValue.emailId.length !== 0 && formValue.password.length !== 0) {
      if (this.validateEmail(formValue.emailId)) {
        this.storage.deleteItem('token');
        this.signInManagerService.getJwtTokenForLogin(formValue).subscribe((res: any) => {

          if (res.access_token) {
            console.log(res.access_token);
            this.storage.addItem('token', res.access_token);
            let jwtPayload=this.signInManagerService.getUserInfo(res.access_token);
            console.log(jwtPayload);

            let userRole = jwtPayload.authorities[0];

            if ( userRole === 'ROLE_THERAPIST') {
              this.storage.addItem('role', userRole);

            } else if (userRole === 'ROLE_PATIENT'){
              this.signInManagerService.getPatientByUsername(jwtPayload.user_name)
              .subscribe(res => {
                if (res.id ) {
                  this.storage.addItem('signedIn', true);
                  this.storage.addItem('patientId', res.id);
                  this.storage.addItem('emailId', res.username);
                  this.storage.addItem('password', formValue.password);
                  this.storage.addItem('firstName',res.firstName);
                  this.storage.addItem('lastName', res.lastName);
                  this.storage.addItem('dateOfBirth', res.dateOfBirth);
                  this.storage.addItem('weight', res.weight);
                  this.storage.addItem('height', res.height);
                  this.storage.addItem('gender', res.gender);
                  this.storage.addItem('mobileNumber', res.mobileNumber);

                  this.presentSingleButtonAlert('Alert', 'you Have Successfully Logged In.', 'Ok');
                  this.getBuyId(res.id);
                  //this.setUpAfterSignUp();
                  this.moveToHome();
                    } else {
                      this.presentSingleButtonAlert('Alert', 'Something went wrong! please try again.', 'Ok');
                    }

              }), (error)=> {
                // handle error
              };


            } else {
                // no role defile
            }



          }
        }, (error) => {
          this.presentSingleButtonAlert('Alert', 'Something went wrong! please try again.', 'Ok');
         });

      } else {
        this.presentSingleButtonAlert('Alert', 'Please Enter Valid Email Id', 'Ok');
      }
    } else {
      this.presentSingleButtonAlert('Alert', 'Please fill all fields', 'Ok');
     }

}

validateEmail(email) {
  // tslint:disable-next-line: max-line-length
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

moveToProfilePage() {
  this.navCtrl.navigateRoot(['profile']);
}

moveToHome() {
  this.navCtrl.navigateRoot(['home']);
}


presentSingleButtonAlert(alertTitle, alertMsg, buttonsTitle) {
  this.alertCtrl.create({
    header: alertTitle,
    message: alertMsg,
    buttons: [
      {
        text: buttonsTitle,
        role: 'Ok'
      }
  ]
})
.then(alertEl => {
  alertEl.present();
});
}

signUpButtonClicked() {
  this.navCtrl.navigateForward(['sign-up']);
  }

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
}

ngOnDestroy(): void {
  try {
    this.pageSubArray.forEach(sub => sub.unsubscribe());

  } catch (error) {
    console.log('error while unsubscribing:', error);
  }
}

}
