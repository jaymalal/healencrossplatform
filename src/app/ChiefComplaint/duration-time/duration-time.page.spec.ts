import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DurationTimePage } from './duration-time.page';

describe('DurationTimePage', () => {
  let component: DurationTimePage;
  let fixture: ComponentFixture<DurationTimePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DurationTimePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DurationTimePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
