import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../shared/Services/storage.service';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-duration-time',
  templateUrl: './duration-time.page.html',
  styleUrls: ['./duration-time.page.scss'],
})
export class DurationTimePage implements OnInit {

  duration = 60;
  isTimeSelected = false;
  constructor(private storage: StorageService,
    private nav: NavController) { }

  ngOnInit() {
  }

  setValue(val: number) {
    this.isTimeSelected = true;
    this.duration = val;
    this.storage.addItem('time', '' + this.duration);
  }

  navigateToPMExercise() {
    this.nav.navigateForward('test-exef-list');
  }

}
