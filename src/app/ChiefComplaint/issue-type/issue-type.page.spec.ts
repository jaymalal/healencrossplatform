import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueTypePage } from './issue-type.page';

describe('IssueTypePage', () => {
  let component: IssueTypePage;
  let fixture: ComponentFixture<IssueTypePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssueTypePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueTypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
