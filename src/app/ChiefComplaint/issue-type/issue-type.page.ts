import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ComplaintInfoService } from '../../shared/Services/complaint-info.service';
import {  NavController } from '@ionic/angular';
import { StorageService } from '../../shared/Services/storage.service';

@Component({
  selector: 'app-issue-type',
  templateUrl: './issue-type.page.html',
  styleUrls: ['./issue-type.page.scss'],
})
export class IssueTypePage implements OnInit {

  // Variables
  patientId: any;
  showBlock = false;
  public formsModuleObj = '';


  constructor(
    private formBuilder: FormBuilder,
    private complaintInfo: ComplaintInfoService,
    public navCtrl: NavController,
    private storageService: StorageService) { }


  issueTypeForm = this.formBuilder.group({
    issueType: ['none', [Validators.required]]
  });

  ngOnInit() {
    this.patientId = this.storageService.getItem('patientId');
  }
  getSelection() {

    console.log(this.issueTypeForm.value.issueType);
    if (this.issueTypeForm.value.issueType === 'ORTHO_ISSUE') {  // user selected issue type is ortho type
      this.showBlock = true;
      this.complaintInfo.issueType.next('ORTHO_ISSUE');
    } else {
      // user selected issue type other than ortho type
      this.showBlock = false;
      this.complaintInfo.issueType.next(this.issueTypeForm.value.issueType);
      this.complaintInfo.setChiefArea('');
    }
  }

  loadNextPage() {
    if (this.showBlock) {
    console.log('go to next page');
    this.navCtrl.navigateForward('area-selection');

    } else {
      console.log('can not go to next page');
    }
  }
}
