import { Component, OnInit } from '@angular/core';
import { ComplaintInfoService } from '../../shared/Services/complaint-info.service';
import {  NavController } from '@ionic/angular';
@Component({
  selector: 'app-sub-area-selection',
  templateUrl: './sub-area-selection.page.html',
  styleUrls: ['./sub-area-selection.page.scss'],
})

export class SubAreaSelectionPage implements OnInit {

   // Arrays
   subAreaList: any = [];
   subAreaSideList: any = [];
   selectedSubArea = [];
   subAreaWithSide = [];
   subAreaCodes: any[];
  // Boolean
   refresh = false;
  refresh2 = false;
  isSubAreaClicked = false;
  // Variables
  config: {};
  subArea = '';
  subAreaSide = '';
  constructor(private complaintInfo: ComplaintInfoService,
    public navCtrl: NavController) { }

  ngOnInit() {
      // get Observales current values
    // this.complaintInfo.getSubAreaObs.subscribe(arg => {
    //   this.subAreaList = arg;
    //   this.subAreaList2 = arg;
    // });
  }

  selectState(e) {
    const id = e.target.id;
    const element = document.getElementById(id);
    console.log(element);
    console.log('click element id :' + id);
    if (this.subAreaWithSide.includes(id)) {
      this.isSubAreaClicked = false;
      const i = this.subAreaWithSide.indexOf(id);
      this.subAreaWithSide.splice(i, 1);

      this.subAreaList = [];
      this.subAreaSideList = [];

      for (const iterator of this.subAreaWithSide) {
        let subAreaId; let subAreaSide;
        const numArray = iterator.match(/[0-9]+/gi);
        if ( numArray ) { subAreaId = +numArray[0]; }
        const charaArr = iterator.match(/[A-Z]/gi);
        if ( charaArr ) { subAreaSide = charaArr[0]; }
        if (subAreaId) {
          this.subAreaList.push(subAreaId);
          this.subAreaSideList.push(subAreaSide ? subAreaSide : 'B');

        } else {
          console.log('sub area id not found');

        }
      }
    } else {
      this.subAreaWithSide.push(id);
      this.isSubAreaClicked = true;
      let subAreaId; let subAreaSide;
      const numArray = id.match(/[0-9]+/gi);
      if ( numArray ) { subAreaId = +numArray[0]; }
      const charaArr = id.match(/[A-Z]/gi);
      if ( charaArr ) { subAreaSide = charaArr[0]; }
      if (subAreaId) {
        this.subAreaList.push(subAreaId);
        this.subAreaSideList.push(subAreaSide ? subAreaSide : 'B');

      } else {
        console.log('sub area id not found');
      }
    }

    // this.sub_area = this.subAreaList.toString();
    // this.subArea_side = this.subAreaSideList.toString();
    this.complaintInfo.subArea.next(this.subAreaList);
    this.complaintInfo.subAreaSide.next(this.subAreaSideList);
    element.classList.toggle('active');
  }

  moveToComplaintCreationPage() {
    if (this.isSubAreaClicked) {
      this.navCtrl.navigateForward('chief-complaint-detection');
    }
  }


}
