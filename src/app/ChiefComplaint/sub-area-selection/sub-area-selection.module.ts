import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SubAreaSelectionPage } from './sub-area-selection.page';
import { MaphilightModule } from 'ng-maphilight';
import { MatSliderModule } from '@angular/material/slider';

const routes: Routes = [
  {
    path: '',
    component: SubAreaSelectionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaphilightModule,
    MatSliderModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SubAreaSelectionPage]
})
export class SubAreaSelectionPageModule {}
