import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChiefComplaintDetectionPage } from './chief-complaint-detection.page';

describe('ChiefComplaintDetectionPage', () => {
  let component: ChiefComplaintDetectionPage;
  let fixture: ComponentFixture<ChiefComplaintDetectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChiefComplaintDetectionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChiefComplaintDetectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
