import { Component, OnInit, OnDestroy } from '@angular/core';
import { ComplaintInfoService } from '../../shared/Services/complaint-info.service';
import { StorageService } from '../../shared/Services/storage.service';
import { SessionManagerService } from '../../shared/Services/session-manager.service';
import { Session } from '../../shared/module/Session';
import {  NavController } from '@ionic/angular';
import { Subscription } from 'rxjs';

export interface SubArea {
  subArea: number;
  pic: number;
  side: string;
}

export interface Sidepic {
  pic: number;
  side: string;
}

export interface ChiefComplaintDto {
   mainArea: string;
   mainPic: number;
   sidepic: Sidepic[];
   subAreas: SubArea[];
   userSelections: string[];
 }

@Component({
  selector: 'app-chief-complaint-detection',
  templateUrl: './chief-complaint-detection.page.html',
  styleUrls: ['./chief-complaint-detection.page.scss']
})


export class ChiefComplaintDetectionPage implements OnInit , OnDestroy {
  // Array
  // boolen
  isSubmitted = false;
  // Variables
  areaCode: any;
  mainAreaSide: number;
  subAreaArray = [];
  subAreaSideArray = [];
  mainAreaPic: number;
  bfnp = 1;
  patientId: any;
  buyId: any;
  complaintId: any;
  complaintnumber: any;
  regionCode: 1;
  chiefComplaintList: any;
  sessionObj: Session;
  issueType: any;
  chiefComplaintDto: ChiefComplaintDto;

  private pageSubArray: Subscription[] = [];

public set pageSub$(value: Subscription) {
  this.pageSubArray.push(value);
}

  constructor(
    private complaintInfoService: ComplaintInfoService,
    private storage: StorageService,
    private sessionManager: SessionManagerService,
    public navCtrl: NavController
  ) {}


  ngOnInit() {
    this.storage.getItem('patientId').then((response: any) => {
      console.log(response);
      this.patientId = response;
      this.getComplaintsByPatientId();
    });

    this.pageSub$ = this.complaintInfoService.getIssueType.subscribe(resp => {
      this.issueType = resp;
    });

    this.pageSub$ = this.complaintInfoService.getIntensity.subscribe(res => {
      this.mainAreaPic = res;
    });

    this.pageSub$ = this.complaintInfoService.getChiefArea.subscribe(res => {
      this.areaCode = res;
    });

    this.pageSub$ = this.complaintInfoService.getSubArea.subscribe(subArea => {
      this.subAreaArray = subArea;
    });

    this.pageSub$ = this.complaintInfoService.getMainAreaSide.subscribe(side => {
      this.mainAreaSide = side;
    });

    this.pageSub$ = this.complaintInfoService.getSubAreaSide.subscribe(subAreaSideArray => {
      this.subAreaSideArray = subAreaSideArray;
    });

    this.storage.getItem('buyId').then((response: any) => {
      this.buyId = response;
      console.log('buyId:', this.buyId, 'response', response);
  });
  }


  getComplaintsByPatientId() {
    console.log(this.patientId);
    this.pageSub$ = this.complaintInfoService.getAllComplaintInfoByPatientId(this.patientId).subscribe(cc => {
      this.chiefComplaintList = cc;
      console.log(this.chiefComplaintList);
      if (this.chiefComplaintList[0]) {
       // this.storage.addItem('complaintId', this.chiefComplaintList[0].id);
        this.complaintnumber = this.chiefComplaintList.length;
        this.complaintnumber = this.complaintnumber + 1;
        console.log('complaint Number:', this.complaintnumber);
      } else {
        this.complaintnumber = 1;
        console.log('complaint Number:', this.complaintnumber);
      }
    });
  }

  createChiefComplaint() {
    if (this.subAreaArray.length === 0) {
      return;
    }
    let chiefComplaintDto: any;

    chiefComplaintDto = this.getChiefComplaintDto();

    this.pageSub$ = this.complaintInfoService.saveBodyAreaSelections(this.patientId, this.buyId,
      this.regionCode, this.areaCode, chiefComplaintDto, true, this.issueType
      ).subscribe(createComplaintResult => {
        this.storage.addItem('complaintId', createComplaintResult.result.id);
        this.storage.addItem('tac', createComplaintResult.result.tac);
        console.log('CreateComplaintResponse:', createComplaintResult);
        this.getSessionInfoFromUserId(this.patientId);
      });
  }

  getChiefComplaintDto() {

    let mainAreaSideStr: string;
    if (this.mainAreaSide === 0) {
      mainAreaSideStr = 'LEFT';
    } else {
      mainAreaSideStr = 'RIGHT';
    }

    // tslint:disable-next-line: prefer-const
    let subAreaDicArray: any[] = [];

    if (this.mainAreaPic === 0 ) {
      this.mainAreaPic = 5;
    }
    for (const key in this.subAreaArray) {
      if (this.subAreaArray.hasOwnProperty(key)) {
        const element = this.subAreaArray[key];

        let sideVal = this.subAreaSideArray[key];
        if (sideVal === 'R') {
          sideVal = 'RIGHT';
        } else if (sideVal === 'L') {
          sideVal = 'LEFT';
        } else {
          sideVal = 'BILATERAL';
        }
        const subAreaDic: SubArea = {
          side: sideVal,
          pic: this.mainAreaPic,
          subArea: element
        };
        subAreaDicArray.push(subAreaDic);
      }
    }


    this.chiefComplaintDto =  {
      mainArea: this.areaCode,
      mainPic: this.mainAreaPic,
      sidepic: [{side: mainAreaSideStr, pic : this.mainAreaPic}],
      subAreas: subAreaDicArray,
      userSelections: [this.areaCode]
    };

    return this.chiefComplaintDto;
  }
  // To check is session already exist or need to create new one
  getSessionInfoFromUserId(userId: any) {

    this.pageSub$ = this.sessionManager.GetSessionInfoFromId(userId).subscribe((sessionInfo: any) => {

      this.sessionObj = sessionInfo;
      if ( this.sessionObj.id > 0 ) {
        this.storage.addItem('sessionId', this.sessionObj.id);
        this.navCtrl.navigateForward('medical');
      } else {
          this.createSession();
       }
    });
  }

  // To create new session
  createSession() {
    this.pageSub$ = this.sessionManager.createNewSession(this.patientId, this.buyId).subscribe((sessionInfo: any) => {
    this.sessionObj = sessionInfo.result;
    this.storage.addItem('sessionId', this.sessionObj.id);
    this.navCtrl.navigateForward('medical');
    });

  }

  ngOnDestroy(): void {
    try {
      this.pageSubArray.forEach(sub => sub.unsubscribe());

    } catch (error) {
      console.log('error while unsubscribing:', error);
    }
  }

}
