import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { ChiefComplaintDetectionPage } from './chief-complaint-detection.page';


const routes: Routes = [
  {
    path: '',
    component: ChiefComplaintDetectionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ChiefComplaintDetectionPage]
})
export class ChiefComplaintDetectionPageModule {}
