import { Component, OnInit } from '@angular/core';
import { ComplaintInfoService } from '../../shared/Services/complaint-info.service';
import {  NavController } from '@ionic/angular';

@Component({
  selector: 'app-area-selection',
  templateUrl: './area-selection.page.html',
  styleUrls: ['./area-selection.page.scss'],
})
export class AreaSelectionPage implements OnInit {

  // Arrays
  areaFrontList: any[];
  areaBackList: any[];
  // Boolean
  showSubArea = false;
  isrefresh1 = true;
  isrefresh2 = true;
  // Variables
  areaCode: any;
  side: number;
  painIntensity = 5;
  activeArea = '';
  mainarea = '';
  sideselection = '1';

  constructor( private complaintInfo: ComplaintInfoService,
    public navCtrl: NavController) { }

  config = {
    fade: true,
    alwaysOn: false,
    neverOn: false,

    // fill
    fill: true,
    fillColor: '#ffffff',
    fillOpacity: 0.4,

    // stroke
    stroke: true,
    strokeColor: '#4d0ec0',
    strokeOpacity: 1,
    strokeWidth: 1,

    // shadow:
    shadow: true,
    shadowColor: '#000000',
    shadowOpacity: 0.8,
    shadowRadius: 10
  };

  ngOnInit() {

    // get Observales current values
    // this.complaintInfo.getAreaFront.subscribe(res => {
    //   this.areaFrontList = res;
    // });

    // this.complaintInfo.getAreaBack.subscribe(res => {
    //   this.areaBackList = res;
    // });

    this.complaintInfo.getChiefArea.subscribe(res => {
      this.areaCode = res;
    });
    this.complaintInfo.mainAreaSide.subscribe(side => {
      this.side = side;
    });
  }

  // onsvgSelect(val: string, side: number, status: boolean, alter?: string) {
  //   console.log('val:', val, 'side:', side, 'status', status, 'alter', alter);
  //   this.complaintInfo.setChiefArea(val);
  //   this.complaintInfo.side.next(0);
  //   this.areaBackList = this.areaBackList.map((iterator) => {
  //     const i = iterator;
  //     i.status = false;
  //     return i;
  //   });
  //   const tempArr = this.complaintInfo.AreaList;
  //   for (const iterator of tempArr) {
  //     iterator.status = false;
  //     iterator.alter = 'H';
  //     if (iterator.name === val) {
  //       iterator.status = status;
  //       if (alter) {
  //         iterator.alter = alter;
  //       }
  //     }
  //   }
  //   this.areaFrontList = tempArr;
  //   this.complaintInfo.areaFrontSub.next(tempArr);
  //   this.isrefresh1 = false;
  //   this.isrefresh2 = false;
  //   setTimeout(() => {
  //     this.isrefresh1 = true;
  //     this.isrefresh2 = true;
  //   }, 1);
  //   if (val === 'B') {
  //     this.showSubArea = true;
  //   } else {
  //     this.showSubArea = false;
  //   }
  // }

  // onSvgSelectBack(val: any, side: any, status: boolean) {
  //   console.log('val:', val, 'side:', side, 'status', status);
  //   this.areaFrontList = this.areaFrontList.map((e) => {
  //     const i = e;
  //     i.status = false;
  //     return i;
  //   });
  //   console.log(val);
  //   this.complaintInfo.side.next(1);
  //   const tempArr = this.complaintInfo.areaBackList;
  //   for (const iterator of tempArr) {
  //     iterator.status = false;
  //     if (iterator.name === val) {
  //       iterator.status = status;
  //       this.complaintInfo.setChiefArea(iterator.area);
  //     }
  //   }
  //   this.areaBackList = tempArr;
  //   this.complaintInfo.areaBackSub.next(tempArr);
  //   this.isrefresh1 = false;
  //   this.isrefresh2 = false;
  //   setTimeout(() => {
  //     this.isrefresh1 = true;
  //     this.isrefresh2 = true;
  //   }, 1);
  //   if (val === 'B') {
  //     this.showSubArea = true;
  //   } else {
  //     this.showSubArea = false;
  //   }
  // }

  loadNextPage() {
    if (this.showSubArea) {
      this.navCtrl.navigateForward('sub-area-selection');
    }
  }

  sliderValueChanged(newRangeValue) {
    console.log(newRangeValue.detail.value);
    this.complaintInfo.painIntensity.next(newRangeValue.detail.value);
  }

  setActive( area: string , index: string, side = 'B' ) {
    if ( area === this.activeArea && side === this.sideselection) {
      this.activeArea = '';
    } else {
      this.activeArea = area;
      this.mainarea = index;
      this.sideselection = side;
      this.showSubArea = true;
      this.complaintInfo.chiefArea.next(this.mainarea);
      this.complaintInfo.mainAreaSide.next(this.side);
    }

  }
}
