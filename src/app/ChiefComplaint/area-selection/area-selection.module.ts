import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { MaphilightModule } from 'ng-maphilight';
import { IonicModule } from '@ionic/angular';
import { MatSliderModule } from '@angular/material/slider';
import { AreaSelectionPage } from './area-selection.page';

const routes: Routes = [
  {
    path: '',
    component: AreaSelectionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaphilightModule,
    MatSliderModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AreaSelectionPage]
})
export class AreaSelectionPageModule {}
