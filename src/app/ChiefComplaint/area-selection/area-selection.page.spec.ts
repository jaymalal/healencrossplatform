import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaSelectionPage } from './area-selection.page';

describe('AreaSelectionPage', () => {
  let component: AreaSelectionPage;
  let fixture: ComponentFixture<AreaSelectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaSelectionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaSelectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
