import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ComplaintExercise, CurrentProgression } from '../../shared/module/exercise';
import { Subscription, from } from 'rxjs';
import { ExerciseManagerService } from '../exercise-manager.service';
import { SessionManagerService } from '../../shared/Services/session-manager.service';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/shared/Services/storage.service';

@Component({
  selector: 'app-video-display',
  templateUrl: './video-display.page.html',
  styleUrls: ['./video-display.page.scss'],
})
export class VideoDisplayPage implements OnInit {

  src = 'https://aktive-health.s3.ap-south-1.amazonaws.com/assets/session/901/1276/1276_1avr';

  @ViewChild('video', { static: true }) videoTag: ElementRef;
  video: HTMLVideoElement;
  videoSub$: Subscription;
  progression: CurrentProgression;
  exercise: ComplaintExercise;


  constructor(private exerciseManager: ExerciseManagerService,
    private storageService: StorageService,
    private sessionManager: SessionManagerService,
    private router: Router) { }

  ngOnInit() {
    this.exercise = this.exerciseManager.currentExercise;

    this.progression = this.exerciseManager.progression;


    // this.videoSub$ = this.exerciseManager.prepareVideo(this.exercise.id, this.progression.progressionReps).subscribe(
    //   res => {
    //     this.src = res;
    //   }
    // );

    this.src = 'https://aktive-health.s3.ap-south-1.amazonaws.com/assets/videos/0004IVR.mp4'; // prepare video call is
    // not working so temp added static url;

  }

  getFeedback() {

    try {
      this.video.pause();

    } catch (e) {
      console.log(e);

    }
    this.exerciseManager.currentSetsDone = this.exerciseManager.currentSetsDone + 1;
    const state = this.sessionManager.feedbackState;
    this.sessionManager.feedbackState = {
      ceId: state.ceId,
      maxSets: state.maxSets,
      setsDone: state.setsDone + 1
    };
    this.router.navigate(['exef-feedback']);

  }


}
