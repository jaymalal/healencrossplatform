import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoDisplayPage } from './video-display.page';

describe('VideoDisplayPage', () => {
  let component: VideoDisplayPage;
  let fixture: ComponentFixture<VideoDisplayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoDisplayPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoDisplayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
