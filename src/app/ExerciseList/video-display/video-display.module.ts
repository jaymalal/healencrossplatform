import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatVideoModule } from 'mat-video';
import { IonicModule } from '@ionic/angular';

import { VideoDisplayPage } from './video-display.page';

const routes: Routes = [
  {
    path: '',
    component: VideoDisplayPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatVideoModule,
    RouterModule.forChild(routes)
  ],
  declarations: [VideoDisplayPage]
})
export class VideoDisplayPageModule {}
