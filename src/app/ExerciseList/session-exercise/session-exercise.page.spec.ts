import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionExercisePage } from './session-exercise.page';

describe('SessionExercisePage', () => {
  let component: SessionExercisePage;
  let fixture: ComponentFixture<SessionExercisePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SessionExercisePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionExercisePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
