import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { StorageService } from '../../shared/Services/storage.service';
import { BucketListManagerService } from '../../shared/Services/bucket-list-manager.service';
import { ComplaintInfoService } from '../../shared/Services/complaint-info.service';
import {  NavController } from '@ionic/angular';
import {  ViewChild, ElementRef } from '@angular/core';
import { SessionManagerService } from '../../shared/Services/session-manager.service';
import { Router } from '@angular/router';
import { ComplaintExercise, CurrentProgression, Exercise } from '../../shared/module/exercise';
import { ExerciseManagerService } from '../../ExerciseList/exercise-manager.service';
import { ExerciseProgrationService } from '../../shared/exercise-progration.service';
import { ExerciseSelectionService } from '../exercise-selection.service';


@Component({
  selector: 'app-session-exercise',
  templateUrl: './session-exercise.page.html',
  styleUrls: ['./session-exercise.page.scss'],
})
export class SessionExercisePage implements OnInit, OnDestroy, OnChanges {

 @ViewChild('execBtn', { static: true }) modalBtn: ElementRef;
  constructor(private storageService: StorageService,
   private bucketListService: BucketListManagerService,
   private complaintInfo: ComplaintInfoService,
   private router: Router,
   private sessionManager: SessionManagerService,
   private exerciseManager: ExerciseManagerService,
   public navCtrl: NavController,
   public exerciseProgrationService: ExerciseProgrationService,
   public exerciseSelectionService: ExerciseSelectionService) { }

  complaintId: any;
  doesHaveSessionId = false;
  sessionDuration: any;
  sessionId: any;
  insertDone: any;
  loading = true;
  ExecDone = 0;
  isInsertExec: any;
  videoUrl: any;
  time: any;
  isInsertDone = false;

  exerciseList: ComplaintExercise[];
  insertExercise: ComplaintExercise[] = [];
  private pageSubscription: Subscription[] = [];
  public set pageSubscription$(value: Subscription) {
    this.pageSubscription.push(value);
  }
  exec = [];

  current = 0;

  ngOnInit() {

    this.storageService.getItem('complaintId').then((response: any) => {
      this.complaintId = response;
     });

    this.storageService.getItem('storage').then((response: any) => {
      this.sessionDuration = response;
     });

    this.storageService.getItem('time').then((time: any) => {
      this.time = time;
    });


    this.storageService.getItem('sessionId').then((response: any) => {
      console.log('sessionId On exerciseList Page : ', response);
      this.sessionId = response;

      this.pageSubscription$ = this.sessionManager.getExercisesBySessionId().subscribe(
        res => {
          console.log(res);
          this.doesHaveSessionId = true;
          if (res.length < 1) {
            this.getSessionExercises();
            this.getSessionExerciseList(res);
            this.loading = false;
            this.getSessionExercisesCount();

          } else {
           this.getSessionExerciseList(res);
           this.loading = false;
           this.getSessionExercisesCount();
          }
        }
      );
     });

    this.current = this.bucketListService.currentExec;
  }

  ionViewWillEnter() {

    if (this.doesHaveSessionId) {
      this.pageSubscription$ = this.sessionManager.getExercisesBySessionId().subscribe(
        res => {
          console.log(res);
          this.doesHaveSessionId = true;
          if (res.length < 1) {
            this.getSessionExercises();
            this.getSessionExerciseList(res);
            this.loading = false;
            this.getSessionExercisesCount();
          } else {
           this.getSessionExerciseList(res);
           this.loading = false;
           this.getSessionExercisesCount();
          }
        }
      );
    }

  }
  ngOnChanges() {

  if (this.doesHaveSessionId) {
    this.pageSubscription$ = this.sessionManager.getExercisesBySessionId().subscribe(
      res => {
        console.log(res);
        this.doesHaveSessionId = true;
        if (res.length < 1) {
          this.getSessionExercises();
          this.getSessionExerciseList(res);
          this.loading = false;
          this.getSessionExercisesCount();

        } else {
         this.getSessionExerciseList(res);
         this.loading = false;
         this.getSessionExercisesCount();
        }
      }
    );
  }

  }

  getSessionExercises() {

    this.insertDone = this.sessionManager.isInsertDone;
    this.pageSubscription$ = this.exerciseManager.getPrevSessionId().subscribe(
      exec => {

        this.pageSubscription$ = this.sessionManager.getCurrentSessioNum(this.sessionId).subscribe(
          srNum => {
            // tslint:disable-next-line: max-line-length
            this.pageSubscription$ = this.exerciseSelectionService.getSessionExercisesByWeightage(+this.time, exec.exec, srNum).subscribe(
              res => {
                const bucketList = res;
                this.exerciseList = res;
                const ceId = [];
                for (const i of res) {
                  ceId.push(i.id);
                }
                this.exerciseManager.sessionBucketList$.next(bucketList);

                this.pageSubscription$ = this.exerciseManager.saveSessionExercises(ceId).subscribe(
                  status => {
                    this.exerciseManager.sessionBucketList$.next(bucketList);

                  }, () => {
                  }
                );
              },
              () => {
              }
            );

          });
      },
      e => {
      }
    );

  }

  // getInsertExerciseList(res: ComplaintExercise[]) {

  //   this.insertExercise = [];
  //   for (const exec of res) {
  //     if (exec.insertExercise) {
  //       this.insertExercise.push(exec);
  //     }
  //   }
  // }

  getSessionExerciseList(res: ComplaintExercise[]) {

    this.exerciseList = [];
    for (const exec of res) {
      if (!exec.insertExercise) {
        this.exerciseList.push(exec);
      }
    }
  }


  getSessionExercisesCount() {

    this.pageSubscription$ = this.sessionManager.getDoneExercisesBySessionId().subscribe(
      res => {

        let count = 0;
        for (const exec of res) {

          if (!exec.insertExercise && (exec.exercise.exef !== 'PM' && exec.exercise.exef !== 'SL')) {
            count += 1;
          }
        }
        this.ExecDone = count;
      }
    );
  }

  getCCList(item): string {
    return item.ceBelongsToCCs.toString();
  }



  endSession() {
    this.bucketListService.execBucketList = [];
    this.current = 0;
    this.bucketListService.currentExec = 0;
    this.pageSubscription$ = this.sessionManager.closeSession(this.sessionId).subscribe(res => {
      this.navCtrl.navigateRoot(['home']);
    },
      e => {
        console.log('error while closing session => ' + this.sessionId);
      });

  }


  // PlayVideo(complaintExerId: any) {

  //   console.log('exercise list:', this.exec);
  //   const obj = this.exec.find(x => x.id === complaintExerId);
  //   console.log('object:', obj);

  //   this.storageService.addItem('exefType', obj.exercise.exef);
  //   this.storageService.addItem('complaintExerId', complaintExerId);
  //   this.current += 1;
  //   this.bucketListService.currentExec += 1;
  //   this.navCtrl.navigateForward(['video-display']);
  // }



  showIntroModal(exercise, isInsert) {

    this.exerciseManager.playVideo.next(true);
    this.isInsertExec = isInsert;
    // if (this.isInsertExec) {
    //   if (this.insertDone >= this.insertExercise.length - 1) {
    //     this.insertExercise = [];
    //     this.sessionManager.isInsertDone = true;
    //   }
    // }
    this.exerciseManager.currentExercise = exercise;
    this.navCtrl.navigateForward(['intro-video']);
  }


  // getExercisesByChiefComplaintId() {
  //   this.complaintInfo.getExercisesByChiefCOmplaintId(this.complaintId)
  //     .subscribe(res => {
  //       this.exec = res;
  //       this.bucketListService.mainBucketList = res;

  //       //  Sort exercises as per type
  //       this.exec = this.exec.sort((a, b) => {
  //         return a.exercise.exef.localeCompare(b.exercise.exef);
  //       });

  //       // get session exercises list
  //       this.exec = this.bucketListService.getDeliverExerciesList(+this.sessionDuration, 'Beginning', true, this.exec);
  //       this.exec = [...new Set(this.exec)];
  //       this.bucketListService.execBucketList = this.exec;
  //       this.bucketListService.sessionExecList = this.exec;

  //     });
  // }


  ngOnDestroy(): void {
    this.ExecDone = 0;
    try {
      this.pageSubscription.forEach(sub => sub.unsubscribe());
    } catch (error) {
      console.log('error while unsubscribing:', error);
    }
  }

}
