import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { baseUrl } from '../shared/Constants/constant';
import { HttpClient } from '@angular/common/http';
import { ComplaintExercise } from '../shared/module/exercise';
import { Observable, BehaviorSubject, from } from 'rxjs';
import { SessionManagerService } from '../shared/Services/session-manager.service';
import { StorageService } from '../shared/Services/storage.service';
import { Session } from '../shared/module/Session';
import { ChiefComplaint } from '../shared/module/exercise';
import { ChiefComplaintRes } from '../shared/module/cheifComplaint';

@Injectable({
  providedIn: 'root'
})
export class ExerciseManagerService {

  constructor(private  http: HttpClient,
    private sessionManagerService: SessionManagerService,
    public storageService: StorageService) {

      this.storageService.getItem('patientId').then((response: any) => {
        this.patientId = response;
      });

      this.storageService.getItem('sessionId').then((response: any) => {
        console.log('session Id:', response);
        this.sessionId = response;
      });

      this.storageService.getItem('ccId').then((response: any) => {
         this.ccId = response;
       });

      this.storageService.getItem('execId').then((response: any) => {
        this.execId = response;
      });
    }


  currentSetsDone: number;
  setsToDo: any;
  insertDone: any = 0;
  userSessions: Session[];
  execDone: any = 0;
  currentExercise: ComplaintExercise;
  progression: any;
  patientId: any;
  sessionId: any;
  playVideo =  new BehaviorSubject<boolean>(false);
  ccId: any;
  execId: any;
  getPlayVideo = this.playVideo.asObservable();

  sessionBucketList$ = new BehaviorSubject<ComplaintExercise[]>([]);
  getSessionBucketList$ = this.sessionBucketList$.asObservable();

  getIntroductionVideo(videoCode = 1000) {

    const url = baseUrl + `ortho/educationalVideo/${videoCode}`;
    return this.http.get(url).pipe(
      map(
        (res: any) => res.result
      )
    );
  }

  getPrevSessionId(): Observable<{ exec: ComplaintExercise[], srNo: any }> {

    return this.sessionManagerService.GetSessionInfoFromId(this.patientId).pipe(
      map((res: any) => {
        console.log('get seesion info res', res);
        const id = [];
        for (const i of res.result) {
          if (i.currentStatus !== 'NEW') {
            id.push(i);
          }
        }
        return id.length > 1 ? { exec: id[id.length - 1].exStatuses, srNo: id[id.length - 1].sessionSrNo } : { exec: [], srNo: 0 };
      })

    );
  }

  getinsertExecList(): Observable<ComplaintExercise[]> {

    return this.getChiefComplaintExercise().pipe(map((res: ChiefComplaint[]) => {

      const insertExerciseList: ComplaintExercise[] = [];
      for (const iterator of res) {
        for (const exec of iterator.exercise) {

          if (exec.workflowStatus !== 'DISCONTINUED') {
            if (exec.insertExercise) {
              if (exec.exercise.exef !== 'SL') {
                insertExerciseList.push(exec);
              }
            }
          }
        }
      }
      return insertExerciseList;
    }));

  }

  getChiefComplaintExercise(): Observable<ChiefComplaint[]> {

    const url = baseUrl + `complaint/patient/${this.patientId}`;
    const ccData: ChiefComplaint[] = [];
    return this.http.get<ChiefComplaintRes[]>(url).pipe(
      map((res: any) => res.result),
      map((ex: any[]) => {
        for (const i of ex) {

          const temp: ChiefComplaint = {
            id: i.id,
            weightage: i.weightage || (100 / ex.length),
            exercise: i.complaintExercises,
            phase: i.currentPhase
          };

          ccData.push(temp);
        }
        console.log(ccData);

        return ccData;
      })
    );
  }

  saveSessionExercises(ceId: any[]): Observable<any> {

    const url = baseUrl + `session/session/${this.sessionId}/bucket`;
    const body = {
      ceIds: ceId
    };
    return this.http.post(url, ceId);
  }

  prepareVideo(ceId: string, reps: string): Observable<string> {

    // reps = '1';             // TODO Remove declaration on Production
    const url = baseUrl + `ortho/video/ceId/${ceId}/${reps}`;
    return this.http.get(url).pipe(
      map((res: any) => res.result.accessUrl)
    );
  }


 skipExerciseSaveStatus(ceID: string, execStatus: any): Observable<any> {

   const url = baseUrl + `session/session/${this.sessionId}/chiefcomplaint/${this.ccId}/compliantExercise/${this.ccId}`;
   return this.http.post(url, execStatus);
 }

 getStatusById(sessionExecList: any[], id: any): string {

  const statusArr = ['Done'];
  for (const i of sessionExecList) {

    if (i.complaintExercise.id === id) {
      statusArr.push(i.status);

    }
  }

  return statusArr[statusArr.length - 1];
  }

  getExerciseHistory(): Observable<any> {

    const url = baseUrl + `session/patient/${this.patientId}`;
    return this.http.get(url).pipe(
      map((res: any) => res.result),
      map((session: any[]) => {
        let execList = [];

        for (const i of session) {

          execList = execList.concat(i.exStatuses);
        }
        return execList;
      }

      )
    );
  }
}
