import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroVideoPage } from './intro-video.page';

describe('IntroVideoPage', () => {
  let component: IntroVideoPage;
  let fixture: ComponentFixture<IntroVideoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroVideoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroVideoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
