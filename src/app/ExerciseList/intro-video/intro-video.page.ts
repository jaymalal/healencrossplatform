import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { ExerciseManagerService } from '../exercise-manager.service';
import { Subscription } from 'rxjs';
import { ComplaintExercise, CurrentProgression} from '../../shared/module/exercise';
import { StorageService } from '../../shared/Services/storage.service';
import { Router } from '@angular/router';
import { SessionManagerService } from '../../shared/Services/session-manager.service';
import { ExerciseProgrationService } from '../../shared/exercise-progration.service';
@Component({
  selector: 'app-intro-video',
  templateUrl: './intro-video.page.html',
  styleUrls: ['./intro-video.page.scss'],
})
export class IntroVideoPage implements OnInit, OnDestroy {

  @ViewChild('execBtn', { static: true }) modalBtn: ElementRef;

  video: HTMLVideoElement;
  videoUrl: any;
  activeExec: ComplaintExercise;
  isVideoEnded = false;
  playVideo: boolean;
  isInsertExec: any;
  skipExecList: any = [];


  private pageSubscription: Subscription[] = [];
  public set pageSubscription$(value: Subscription) {
    this.pageSubscription.push(value);
  }

  constructor(private sessionManager: SessionManagerService,
    public exerciseManager: ExerciseManagerService,
    public storageService: StorageService,
    public router: Router,
    private execProgressionServ: ExerciseProgrationService) { }

  ngOnInit() {

    this.activeExec = this.exerciseManager.currentExercise;

    this.exerciseManager.getPlayVideo.subscribe(resp => {
      this.playVideo = resp;
      this.getIntroVideo();
    });

    this.exerciseManager.getExerciseHistory().subscribe(
      res => {
        this.skipExecList = res;
      }
    );

  }


  getIntroVideo() {
    this.pageSubscription$ = this.exerciseManager.getIntroductionVideo(1000).subscribe(
      res => {
        this.videoUrl = res;
      }
    );

  }

  executeExercise(exercise: ComplaintExercise, isInsert: boolean) {
    // this method is picked from sessionExercise (in case of any this.variable)
    this.videoUrl = null;
    this.exerciseManager.playVideo.next(false);
    this.exerciseManager.currentSetsDone = 0;
    this.exerciseManager.setsToDo = exercise.progressionSets || exercise.exercise.setMin;
    isInsert ? this.exerciseManager.insertDone += 1 : this.exerciseManager.execDone += 1;
    // if (this.ExecDone === this.insertExercise.length) {
    //   this.insertExercise = [];
    //   this.exerciseManager.insertDone = 0;
    // }
    this.exerciseManager.currentExercise = exercise;
    const so: CurrentProgression = this.execProgressionServ.getExerciseProgression(exercise);
    this.sessionManager.feedbackState = {
      ceId: exercise.id,
      maxSets: so.progressionSets || exercise.exercise.setMin || 1, // TODO replace with real value on production as so.progressionSets
      setsDone: 0
    };
    this.storageService.addItem('ccId', exercise.ccId);
    this.exerciseManager.progression = so;
    this.router.navigate(['video-display']);
  }

  Done() {
    this.router.navigate(['exef-feedback']);

  }

  skipExercise(activeExec: ComplaintExercise) {

    this.exerciseManager.currentExercise = activeExec;
    this.storageService.addItem('ccId', activeExec.ccId);
    this.storageService.addItem('execId', activeExec.id);

    const exStatus = {
      status: 'EX_SKIPPED'
    };

    this.exerciseManager.skipExerciseSaveStatus(activeExec.id, exStatus).subscribe(
      (res: any) => {
        // TODO Check Logic to check the status of exercise is Skipped or not


        if (this.skipExecList.length > 0) {

          const status = this.exerciseManager.getStatusById(this.skipExecList, activeExec.id);

          if (status !== 'EX_SKIPPED') {
           // this.getData();
          } else {
            this.router.navigate(['questionair/name/fb_skip']);
          }
        } else {
          this.exerciseManager.getExerciseHistory().subscribe(
            skList => {
              const status = this.exerciseManager.getStatusById(skList, activeExec.id);

              if (status !== 'EX_SKIPPED') {
                // this.getData();
              } else {
                this.router.navigate(['questionair/name/fb_skip']);
              }
            }
          );
        }


      }
    );

  }

  ngOnDestroy() {
    try {
      this.pageSubscription.forEach(sub => sub.unsubscribe());
    } catch (error) {
      console.log('error while unsubscribing:', error);
    }
  }
}
