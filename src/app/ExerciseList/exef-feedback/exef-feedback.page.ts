import { Component, OnInit, OnChanges } from '@angular/core';
import { SurveyJsonManagerService } from '../../shared/Services/survey-json-manager.service';
import * as  Survey from 'survey-angular';
import { AlertController } from '@ionic/angular';
import { StorageService } from '../../shared/Services/storage.service';
import { ExerciseManagerService } from '../exercise-manager.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ComplaintExercise } from 'src/app/shared/module/exercise';
import { SessionManagerService } from 'src/app/shared/Services/session-manager.service';

@Component({
  selector: 'app-exef-feedback',
  templateUrl: './exef-feedback.page.html',
  styleUrls: ['./exef-feedback.page.scss'],
})
export class ExefFeedbackPage implements OnInit {

  // surveyJsonData: any;
  // questionaireId: any;
  // patientId: any;
  // complaintId: any;
  // sessionId: any;
  // exefType: any;
  // complaintExerId: any;

  feedbackState: { ceId: string; maxSets: number; setsDone: number; };
  firstFormGroup: FormGroup;
  strenuousGroup: FormGroup;
  exercise: ComplaintExercise;
  surveyId: string;
  surveyJsonData: any;
  questionaireId: any;
  patientId: any;
  sessionId: any;
  complaintId: any;

  constructor(private router: Router,
    private storage: StorageService,
    private surveyJsonService: SurveyJsonManagerService,
    private alertCtrl: AlertController,
    private formBuilder: FormBuilder,
    private exerciseManager: ExerciseManagerService,
    private sessionManager: SessionManagerService) {

    this.exercise = this.exerciseManager.currentExercise;
    this.firstFormGroup = this.formBuilder.group({
      firstQueRadioControl: '',
    });

    }

ngOnInit() {

  this.storage.getItem('sessionId').then((response: any) => {
    this.sessionId = response;
   });

  this.storage.getItem('patientId').then((response: any) => {
    this.patientId = response;
  });

  this.storage.getItem('complaintId').then((response: any) => {
    this.complaintId = response;
   });

  this.surveyId = this.getSurveyJson();
  this.loadSurveyJsonDataOnPage();

  }

  loadSurveyJsonDataOnPage() {
    this.surveyJsonService.getQuestionnaireById(this.surveyId).subscribe((Response: any) => {
      this.surveyJsonData = Response.json;
      this.questionaireId = Response.id;
      console.log(this.surveyJsonData);
      const survey = new Survey.Model('' + this.surveyJsonData);
      survey.data = {
        self: this,
      };
      survey.onComplete.add(this.sendDataToServerAlt);
      survey.onComplete.add(() => {
        survey.clear();
      });
      Survey.SurveyNG.render('surveyJsElementForFB', { model: survey });
      });
  }

  sendDataToServerAlt(sendDataToServerAlt: any) {
    sendDataToServerAlt.data.self.savePatientAnswer(sendDataToServerAlt);
  }

  getValue(value: any) {
    console.log(value);
  }

  savePatientAnswer(surveyAns: any) {
    const result = surveyAns.data;
    delete result.self;
    const userResponse = {
      complaintExerciseId: this.exercise.id,
      complaintId: this.complaintId,
      patientId: this.patientId,
      patientQuestionAnswer: result,
      remedyPackageId: 1,
      sessionId: this.sessionId
    };


    this.sessionManager.saveFeedback(userResponse, this.questionaireId)
      .subscribe(
        (res: any) => {
          const execStatus = {
            cePr: 0,
            ocCounter: 0,
            postExerciseFeedbackId: res.result.id,
            progressionReps: 0,
            progressionSets: 0,
            repsBumped: true,
            repsDone: 0,
            sessionNumber: this.sessionId,
            setsBumped: true,
            setsDone: 0,
            slb: 0,
            soCounter: 0,
            soPainCounter: 0,
            status: 'DONE'
          };
          console.log(res);
          this.sessionManager.saveExerciseStatus(userResponse, execStatus).subscribe(
            () => {
              this.CheckForSetsAndMove();

            },
            e => {
              console.log(e);
              this.CheckForSetsAndMove();

            }
          );
        }
      );
  }

getSurveyJson(): string {
    let surveyName = '';
    switch (this.exercise.exercise.exef) {

      case 'SP': surveyName = '29';
                 break;
      case 'RO': surveyName = '73';
                 break;
      case 'JM': surveyName = '73';
                 break;
      case 'FL': surveyName = '73';
                 break;
      case 'ND': surveyName = '71';
                 break;
      case 'WU':
      case 'SG':
      case 'AN':
      default: surveyName = '72';
               break;

    }
    return surveyName;

  }

moveBackToList() {
  this.router.navigate(['session-exercise']);
}

CheckForSetsAndMove() {
  if (this.exerciseManager.setsToDo > this.exerciseManager.currentSetsDone) {
    this.router.navigate(['video-display']);

  } else {
    this.router.navigate(['session-exercise']);
  }
}


}
