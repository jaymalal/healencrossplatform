import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Session } from '../shared/module/Session';
import { ComplaintExercise, ChiefComplaint } from '../shared/module/exercise';
import { Injectable } from '@angular/core';
import { ExerciseOccurrence } from '../shared/enum/exercise-occurance.enum';
import { map } from 'rxjs/operators';
import { ExerciseManagerService } from './exercise-manager.service';
import { Phase } from '../shared/enum/phase.enum';
import { Observable, from } from 'rxjs';
import { baseUrl } from '../shared/Constants/constant';
import { StorageService } from '../shared/Services/storage.service';
@Injectable({
  providedIn: 'root'
})
export class ExerciseSelectionService {

  bucketList = [];
  baseUrl = baseUrl;
  phase: Phase;
  tempPhase: number;
  patientId: any;
  constructor(
    private http: HttpClient,
    private exerciseManagerService: ExerciseManagerService,
    public storageService: StorageService
  ) {

    this.storageService.getItem('patientId').then((response: any) => {
      this.patientId = response;
    });
  }

  /**
   * @description : check Status in session if present or not
   * @param : exercise : exercise to be searched
   * @param : sessionExercise: list of exercises in session
   * @returns: boolean [ if exercise is present or not ]
   *
   */
  checkExerciseStatus(exercise: ComplaintExercise, sessionExercise: ComplaintExercise[]): boolean {

    return sessionExercise.includes(exercise);
  }

  /**
   * @description : get previous  session of user
   * @returns : Session[]
   */
  getUserPrevSessions() {


    const url = this.baseUrl + `session/patient/${this.patientId}`;

    const sessionList = this.http.get<Session[]>(url).pipe(
      map((res: any) => res.result)
    );

    return sessionList;

  }

  /**
   * @description : get exercise occurrence status in session list
   * @param count : no of sessions to be checked for exercise occurrence
   * @return boolean [ : count] : return array of size `count` with exercise occurrence status
   */
  getExerciseOccurrenceList(exercise: ComplaintExercise, count: number): boolean[] {

    const execStatus = [];
    const userSession = this.exerciseManagerService.userSessions;
    if (userSession) {
      if (userSession.length > 0) {
        for (const iterator of userSession) {
          execStatus.push(this.checkExerciseStatus(exercise, iterator.exStatuses));
        }
        execStatus.reverse();
      }
    }

    if (execStatus.length > count) {
      return execStatus.splice(0, count);
    } else {
      for (let index = execStatus.length; index < count; index++) {
        execStatus.push(false);
      }
      return execStatus;
    }
  }

  /**
   * @description : check Occurrence for exercises with Occurrence OC2
   * @param exercise : ComplaintExercise
   * @returns : boolean // occurrence of exercise
   */
  oc2OccurrenceTest(exercise: ComplaintExercise): boolean {

    const hasOccurrence = true;
    const occList = this.getExerciseOccurrenceList(exercise, 3);

    for (const i of occList) {    // if all 3 session has occurrence then return false
      if (!i) {
        return false;
      }
    }
    return hasOccurrence;   // if any session does not have occurrence then return true;

  }

  /**
   * @description : check Occurrence for exercises with Occurrence OC3
   * @param exercise : ComplaintExercise
   * @returns : boolean // occurrence of exercise
   */
  oc3OccurrenceTest(exercise: ComplaintExercise): boolean {

    const hasOccurrence = false;
    const occList = this.getExerciseOccurrenceList(exercise, 2);

    for (const i of occList) {    // if 2 session has occurrence then return false
      if (!i) {
        return true;
      }
    }
    return hasOccurrence;   // if any session does not have occurrence then return true;

  }


  /**
   * @description : check Occurrence for exercises with Occurrence OC4
   * @param exercise : ComplaintExercise
   * @returns : boolean // occurrence of exercise
   */
  oc4OccurrenceTest(exercise: ComplaintExercise): boolean {
    const occList = this.getExerciseOccurrenceList(exercise, 4);

    if (occList[0]) {
      if (occList[1]) {
        return false;
      } else {
        return true;
      }

    } else {
      if (occList[1]) {
        return false;
      } else {
        return true;
      }
    }

  }


  /**
   * @description : check Occurrence for exercises with Occurrence OC5
   * @param exercise : ComplaintExercise
   * @returns : boolean // occurrence of exercise
   */
  oc5OccurrenceTest(exercise: ComplaintExercise): boolean {

    const hasOccurrence = false;
    const occList = this.getExerciseOccurrenceList(exercise, 1);

    for (const i of occList) {    // if last session does not have occurrence then return true;
      if (!i) {
        return true;
      }
    }
    return hasOccurrence;   // if last session has occurrence then return false


  }

  /**
   * @description : check Occurrence for exercises with Occurrence OC6
   * @param exercise : ComplaintExercise
   * @returns : boolean // occurrence of exercise
   */
  oc6OccurrenceTest( exercise: ComplaintExercise ): boolean {

    const hasOccurrence = false;
    const occList = this.getExerciseOccurrenceList(exercise, 4);

    if (occList[0]) {
      return false;

    } else {
      if (!occList[1]) {
        if (!occList[2]) {
          return true;
        }
      } else {
        return true;
      }
    }
    return hasOccurrence;

  }

  /**
   * @description : check Occurrence for exercises with Occurrence OC7
   * @param exercise : ComplaintExercise
   * @returns : boolean // occurrence of exercise
   */
  oc7OccurrenceTest(exercise: ComplaintExercise): boolean {

    const hasOccurrence = true;
    const occList = this.getExerciseOccurrenceList(exercise, 7);

    for (const i of occList) {    // if 7 session has occurrence then return false
      if (i) {
        return false;
      }
    }
    return hasOccurrence;   // if all sessions does not have occurrence then return true;

  }


  /**
   * @description : check if exercise have occurrence in current session or not
   * @param exercise : exercise
   * @returns : boolean
   */

  checkOccurrence(exercise: ComplaintExercise): boolean {

    const hasOccurrence = false;

    switch (exercise.exercise.oc) {

      case ExerciseOccurrence.OC1: return true;
      case ExerciseOccurrence.OC2: return this.oc2OccurrenceTest(exercise);
      case ExerciseOccurrence.OC3: return this.oc3OccurrenceTest(exercise);
      case ExerciseOccurrence.OC4: return this.oc4OccurrenceTest(exercise);
      case ExerciseOccurrence.OC5: return this.oc5OccurrenceTest(exercise);
      case ExerciseOccurrence.OC6: return this.oc6OccurrenceTest(exercise);
      case ExerciseOccurrence.OC7: return this.oc7OccurrenceTest(exercise);

    }

    return hasOccurrence;
  }

  /**
   * @description: get available slot of exercise by EXEF
   * @param time : user Selected time for session
   * @param exef : exercise type
   * @param phase : current phase of patient
   * @returns : available slots for exef
   */
  getExerciseSlot(time, exef, phs): { start: number, end: number } {

    if (phs === Phase.PHS1) {
      switch (time) {

        case 60: {
          switch (exef) {
            case 'WU': return { start: 5, end: 7 };
            case 'SP': return { start: 2, end: 3 };
            case 'JM': return { start: 2, end: 2 };
            case 'RO': return { start: 3, end: 5 };
            case 'FL': return { start: 3, end: 2 };
            case 'AN': return { start: 2, end: 3 };
            case 'SG': return { start: 3, end: 5 };
            case 'ND': return { start: 2, end: 2 };
            case 'PHY': return { start: 1, end: 1 };

          }
        }
        case 45: {
          switch (exef) {
            case 'WU': return { start: 4, end: 5 };
            case 'SP': return { start: 2, end: 2 };
            case 'JM': return { start: 2, end: 2 };
            case 'RO': return { start: 2, end: 4 };
            case 'FL': return { start: 2, end: 2 };
            case 'AN': return { start: 2, end: 2 };
            case 'SG': return { start: 2, end: 4 };
            case 'ND': return { start: 2, end: 2 };
            case 'PHY': return { start: 1, end: 1 };

          }
        }

        case 30: {
          switch (exef) {
            case 'WU': return { start: 3, end: 4 };
            case 'SP': return { start: 1, end: 2 };
            case 'JM': return { start: 1, end: 1 };
            case 'RO': return { start: 2, end: 3 };
            case 'FL': return { start: 2, end: 1 };
            case 'AN': return { start: 1, end: 2 };
            case 'SG': return { start: 2, end: 3 };
            case 'ND': return { start: 1, end: 1 };
            case 'PHY': return { start: 1, end: 1 };

          }
        }
        case 15: {
          switch (exef) {
            case 'WU': return { start: 1, end: 1 };
            case 'SP': return { start: 1, end: 1 };
            case 'JM': return { start: 1, end: 1 };
            case 'RO': return { start: 1, end: 1 };
            case 'FL': return { start: 1, end: 1 };
            case 'AN': return { start: 1, end: 1 };
            case 'SG': return { start: 1, end: 1 };
            case 'ND': return { start: 1, end: 1 };
            case 'PHY': return { start: 0, end: 0 };

          }
        }
      }
    } else if (phs === Phase.PHS2) {
      switch (time) {

        case 60: {
          switch (exef) {
            case 'WU': return { start: 5, end: 7 };
            case 'SP': return { start: 3, end: 1 };
            case 'JM': return { start: 2, end: 0 };
            case 'RO': return { start: 5, end: 2 };
            case 'FL': return { start: 2, end: 2 };
            case 'AN': return { start: 3, end: 2 };
            case 'SG': return { start: 5, end: 8 };
            case 'ND': return { start: 2, end: 2 };
            case 'PHY': return { start: 1, end: 1 };

          }
        }
        case 45: {
          switch (exef) {
            case 'WU': return { start: 4, end: 5 };
            case 'SP': return { start: 2, end: 1 };
            case 'JM': return { start: 2, end: 0 };
            case 'RO': return { start: 4, end: 2 };
            case 'FL': return { start: 2, end: 2 };
            case 'AN': return { start: 2, end: 2 };
            case 'SG': return { start: 4, end: 6 };
            case 'ND': return { start: 2, end: 2 };
            case 'PHY': return { start: 1, end: 1 };

          }
        }
        case 30: {
          switch (exef) {
            case 'WU': return { start: 3, end: 4 };
            case 'SP': return { start: 2, end: 1 };
            case 'JM': return { start: 1, end: 0 };
            case 'RO': return { start: 3, end: 1 };
            case 'FL': return { start: 1, end: 1 };
            case 'AN': return { start: 2, end: 1 };
            case 'SG': return { start: 3, end: 4 };
            case 'ND': return { start: 1, end: 1 };
            case 'PHY': return { start: 1, end: 1 };

          }
        }
        case 15: {
          switch (exef) {
            case 'WU': return { start: 1, end: 2 };
            case 'SP': return { start: 1, end: 0 };
            case 'JM': return { start: 1, end: 0 };
            case 'RO': return { start: 1, end: 1 };
            case 'FL': return { start: 1, end: 1 };
            case 'AN': return { start: 1, end: 1 };
            case 'SG': return { start: 1, end: 2 };
            case 'ND': return { start: 1, end: 1 };
            case 'PHY': return { start: 0, end: 0 };

          }
        }
      }
    } else if (phs === Phase.PHS3) {
      switch (time) {

        case 60: {
          switch (exef) {
            case 'WU': return { start: 5, end: 7 };
            case 'SP': return { start: 1, end: 1 };
            case 'JM': return { start: 0, end: 0 };
            case 'RO': return { start: 2, end: 0 };
            case 'FL': return { start: 2, end: 3 };
            case 'AN': return { start: 3, end: 2 };
            case 'SG': return { start: 8, end: 7 };
            case 'ND': return { start: 2, end: 2 };
            case 'PHY': return { start: 1, end: 1 };

          }
        }
        case 45: {
          switch (exef) {
            case 'WU': return { start: 4, end: 5 };
            case 'SP': return { start: 1, end: 1 };
            case 'JM': return { start: 0, end: 0 };
            case 'RO': return { start: 2, end: 0 };
            case 'FL': return { start: 2, end: 2 };
            case 'AN': return { start: 2, end: 2 };
            case 'SG': return { start: 6, end: 5 };
            case 'ND': return { start: 2, end: 2 };
            case 'PHY': return { start: 1, end: 1 };

          }
        }

        case 30: {
          switch (exef) {
            case 'WU': return { start: 3, end: 4 };
            case 'SP': return { start: 1, end: 1 };
            case 'JM': return { start: 0, end: 0 };
            case 'RO': return { start: 1, end: 0 };
            case 'FL': return { start: 1, end: 2 };
            case 'AN': return { start: 2, end: 1 };
            case 'SG': return { start: 4, end: 4 };
            case 'ND': return { start: 1, end: 1 };
            case 'PHY': return { start: 1, end: 1 };

          }
        }
        case 15: {
          switch (exef) {
            case 'WU': return { start: 1, end: 2 };
            case 'SP': return { start: 0, end: 0 };
            case 'JM': return { start: 0, end: 0 };
            case 'RO': return { start: 1, end: 0 };
            case 'FL': return { start: 1, end: 1 };
            case 'AN': return { start: 1, end: 1 };
            case 'SG': return { start: 2, end: 2 };
            case 'ND': return { start: 1, end: 1 };
            case 'PHY': return { start: 0, end: 0 };

          }
        }
      }
    }
  }


  /**
   * @description : get list of exercises by EXEF
   * @param exef : EXEF of exercise
   * @param exerciseList: total user exercise list
   * @returns exercise list by  exef
   */
  getExercisesByExef(exef: string, exerciseList: ComplaintExercise[], time: number, phase: string, weightage: number,
     prevExecList: ComplaintExercise[]): ComplaintExercise[] {

    let execList = [];

    // get all exercises with EXEF = exef
    execList = exerciseList.filter((v) => {
      return v.exercise.exef === exef;
    });

    if (execList.length < 1 ) {
      return [];
    }
    const slot = this.getExerciseSlot(time, exef, phase);
    const weight = Math.floor((weightage * slot.start) / 100);
    const weightageSlots = weight === 0 ? 1 : weight;
    const existingExec = this.getCountOfExecInBucketList(exef);

    if (existingExec >= slot.start) {
      return [];
    }

    const filterExercise = execList.filter((v, i, a) => {
      return v.workflowStatus !== 'DISCONTINUED';
    });

    // sort exercises by exercise done or not status
    if (prevExecList.length > 0) {
      filterExercise.sort((a, b) => {
        if (this.checkIfExecIsDone(a, prevExecList)) {
          return -1;
        }
        return 1;
      });
    }


    filterExercise.sort((a, b) => {
      if (a.workflowStatus === 'NEW') { return -1;
      } else {
        return 1;
      }
    });
    for (const exec of filterExercise) {
      if (exec.workflowStatus !== 'NEW') {
        if (!this.checkOccurrence(exec)) {
          filterExercise.splice(filterExercise.indexOf(exec), 1);
        }
      }
    }
    return filterExercise.splice(0, weightageSlots);
  }

  /**
   * @description move Previously done exercises to last , so that new session will get new exercises
   * @param exec
   * @param prevExec
   */

  checkIfExecIsDone( exec: ComplaintExercise, prevExec: ComplaintExercise[]): boolean {
    return prevExec.includes(exec);
  }

  /**
   * @description : check if exercise already exist in bucket list or not
   *                if exist return count of exercises
   * @param: EXEF of exercise
   * @returns: count of EXEF in current exercise
   */
  getCountOfExecInBucketList(exef: string): number {

    let count = 0;
    for (const i of this.bucketList) {
      if (i.exercise.exef === exef) {
        count++;
      }
    }
    return count;
  }

  /**
   * @description : get session bucket list
   * @param phase: current patient phase
   * @param time: time selected by user for session
   * @param exerciseList: total exercise list of user
   * @param weightage: weightage of exercise in percentage
   * @returns complaintExercise[]
   */
  getSessionExercises(phase: string, time: number, exerciseList: ComplaintExercise[],
    weightage = 100, execList: ComplaintExercise[]): ComplaintExercise[] {

    let bucketList = [];

    const exerciseCount = this.getMaxExerciseCount(time, phase);

    // ---------------------------------------------------------------------------//Priority as per progression sheet
    // tslint:disable-next-line: variable-name
    const AN_Exercises = this.getExercisesByExef('AN', exerciseList, time, phase, weightage, execList); // 1
    // tslint:disable-next-line: variable-name
    const SP_Exercises = this.getExercisesByExef('SP', exerciseList, time, phase, weightage, execList); // 2
    // tslint:disable-next-line: variable-name
    const WU_Exercises = this.getExercisesByExef('WU', exerciseList, time, phase, weightage, execList); // 5
    // tslint:disable-next-line: variable-name
    const FL_Exercises = this.getExercisesByExef('FL', exerciseList, time, phase, weightage, execList); // 3
    // tslint:disable-next-line: variable-name
    const RO_Exercises = this.getExercisesByExef('RO', exerciseList, time, phase, weightage, execList); // 5
    // tslint:disable-next-line: variable-name
    const JM_Exercises = this.getExercisesByExef('JM', exerciseList, time, phase, weightage, execList); // 6
    // tslint:disable-next-line: variable-name
    const SG_Exercises = this.getExercisesByExef('SG', exerciseList, time, phase, weightage, execList); // 4
    // tslint:disable-next-line: variable-name
    const ND_Exercises = this.getExercisesByExef('ND', exerciseList, time, phase, weightage, execList); // 5

    // Anchor exercise has highest priority so add AN first
    bucketList = bucketList.concat(AN_Exercises);

    // add sp exercises
    bucketList = bucketList.concat(SP_Exercises);

    // add FL exercises
    bucketList = bucketList.concat(FL_Exercises);

    // add SG exercise
    bucketList = bucketList.concat(SG_Exercises);

    // add RO exercise
    bucketList = bucketList.concat(RO_Exercises);

    // add ND exercise
    bucketList = bucketList.concat(ND_Exercises);

    //  check restriction plane and add WU
    // // TODO: check logic to find whether user has restricted plane or not
    if (WU_Exercises.length > 0) {    // if WU exercises are in bucket list means user does not have Restricted plane.
      bucketList = bucketList.concat(WU_Exercises);
    }

    // add JM exercise
    bucketList = bucketList.concat(JM_Exercises);

    // get count of exercises from selection pool

    return bucketList.splice(0, exerciseCount.start);   /// max_size number of exercises are selected

  }

  /**
   * @description : get start/end count of exercise by phase and time
   * @param time : user selected session time
   * @param phs: phase of patient
   * @returns : start and end count of exercises for phase
   */
  getMaxExerciseCount(time: number, phs: string): { start: number, end: number } {

    if (phs === Phase.PHS1) {
      switch (time) {

        case 60: return { start: 19, end: 20 };
        case 45: return { start: 16, end: 17 };
        case 30: return { start: 10, end: 10 };
        case 15: return { start: 5, end: 5 };
      }
    } else if (phs === Phase.PHS2) {
      switch (time) {

        case 60: return { start: 24, end: 20 };
        case 45: return { start: 20, end: 17 };
        case 30: return { start: 12, end: 10 };
        case 15: return { start: 6, end: 5 };
      }
    } else if (phs === Phase.PHS3) {
      switch (time) {

        case 60: return { start: 16, end: 17 };
        case 45: return { start: 14, end: 14 };
        case 30: return { start: 8, end: 9 };
        case 15: return { start: 4, end: 4 };
      }
    } else if (phs === Phase.PHS4) {
      switch (time) {

        case 60: return { start: 17, end: 18 };
        case 45: return { start: 14, end: 15 };
        case 30: return { start: 9, end: 9 };
        case 15: return { start: 4, end: 5 };
      }
    }
  }


  getSessionExercisesByWeightage(time: number, prevExecList: ComplaintExercise[], sessionNum: any): Observable<ComplaintExercise[]> {

    this.phase = Phase.PHS1;
    let bucketList: ComplaintExercise[] = [];
    return this.exerciseManagerService.getChiefComplaintExercise().pipe(
      map((res: ChiefComplaint[]) => {
        const ccList = res;
        for (const i of ccList) {

          const phs = i.phase === 1 ? Phase.PHS1 : i.phase === 2 ? Phase.PHS2 : i.phase === 3 ? Phase.PHS3 : Phase.PHS4;
          this.phase = this.getMinPhase(i.phase);
          const a = this.getSessionExercises(phs, time, i.exercise, i.weightage, prevExecList);
          bucketList = [...bucketList, ...a];
          this.bucketList = bucketList;
        }
        const total = this.getMaxExerciseCount(time, this.phase);
        if (this.bucketList.length < total.start) {
          for (const i of ccList) {
            const a = this.getSessionExercises(this.phase, time, i.exercise, 100, prevExecList);
            bucketList = [...bucketList, ...a];
            this.bucketList = bucketList;
          }

        }
        this.removeDuplicateExercises();
        //  sort the exercises by EXEF
        const execList = this.sortExercisesByEXEF(this.bucketList, time, this.phase);
        return execList;
      })
    );

  }
  removeDuplicateExercises() {
    this.bucketList = this.bucketList.filter((v, i, a) => a.findIndex(t => (t.id === v.id)) === i
    );
  }
  getMinPhase(phase: any): Phase {

    this.tempPhase = 1;
    if (phase < this.tempPhase) {
      const phs = phase === 1 ? Phase.PHS1 : phase === 2 ? Phase.PHS2 : phase === 3 ? Phase.PHS3 : Phase.PHS4;
      return phs;
    } else {
      const phs = this.tempPhase === 1 ? Phase.PHS1 : this.tempPhase === 2 ? Phase.PHS2 : this.tempPhase === 3 ? Phase.PHS3 : Phase.PHS4;
      return phs;
    }
  }


  sortExercisesByEXEF(bucketList: ComplaintExercise[], time: any, phs: any): ComplaintExercise[] {

    let sortedList: ComplaintExercise[] = [];

    const anExec = this.getExerciseByExef('AN', time);
    const spExec = this.getExerciseByExef('SP', time);
    const flExec = this.getExerciseByExef('FL', time);
    const sgExec = this.getExerciseByExef('SG', time);
    const ndExec = this.getExerciseByExef('ND', time);
    const roExec = this.getExerciseByExef('RO', time);
    const jmExec = this.getExerciseByExef('JM', time);
    const wuExec = this.getExerciseByExef('WU', time);

    const maxExecCount = this.getMaxExerciseCount(time, phs);
    sortedList = sortedList.concat(anExec);
    sortedList = sortedList.concat(spExec);
    sortedList = sortedList.concat(sgExec);
    sortedList = sortedList.concat(ndExec);
    sortedList = sortedList.concat(roExec);
    sortedList = sortedList.concat(jmExec);
    sortedList = sortedList.concat(flExec);
    sortedList = sortedList.concat(wuExec);

    return sortedList.splice(0, maxExecCount.start);   /// max_size number of exercises are selected
  }

  /**
   * @description get list of exercises by EXEF
   * @param exef Exercise type
   * @returns complaintExercise[]
   */
  getExerciseByExef(exef: string, time: any): ComplaintExercise[] {
    const exec: ComplaintExercise[] = [];
    for (const ex of this.bucketList) {

      if (ex.exercise.exef === exef) {
        exec.push(ex);
      }
    }
    const slot = this.getExerciseSlot(time, exef, this.phase);
    return exec.splice(0, slot.start);
  }


}
