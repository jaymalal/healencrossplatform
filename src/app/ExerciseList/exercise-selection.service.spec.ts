import { TestBed } from '@angular/core/testing';

import { ExerciseSelectionService } from './exercise-selection.service';

describe('ExerciseSelectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExerciseSelectionService = TestBed.get(ExerciseSelectionService);
    expect(service).toBeTruthy();
  });
});
