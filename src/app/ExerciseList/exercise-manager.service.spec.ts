import { TestBed } from '@angular/core/testing';

import { ExerciseManagerService } from './exercise-manager.service';

describe('ExerciseManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExerciseManagerService = TestBed.get(ExerciseManagerService);
    expect(service).toBeTruthy();
  });
});
