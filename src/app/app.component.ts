import { Component } from '@angular/core';
import { StorageService, StorageObj } from 'src/app/shared/Services/storage.service';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { from } from 'rxjs';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {


  newStorageObj: StorageObj ;


  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storageService: StorageService,
    private router: Router
  ) {
    this.initializeApp();
    console.log('inside app.componentFile');
  }

  getLoginValue() {
     this.storageService.getItem('signedIn').then((response: any) => {
      console.log('signedInvaule:', response);
      if (response === true) {
        this.router.navigate(['home']);
      } else {
        this.router.navigate(['sign-in']);
      }});


  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.getLoginValue();
    });
  }
}
