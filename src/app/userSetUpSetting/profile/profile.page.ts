import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/shared/Services/storage.service';
import { Patient } from 'src/app/shared/module/Patient';
import { AlertController, NavController } from '@ionic/angular';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {


  patientObj: Patient = new Patient();
  constructor(private storage: StorageService,
    private alertCtrl: AlertController,
    public navCtrl: NavController
    ) { }

  ngOnInit() {
    this.getValues();
  }

  getValues() {

    // get values from storage of all properties
     this.storage.getItem('firstName').then((response: any) => this.patientObj.firstName = response);
     this.storage.getItem('lastName').then((response: any) => this.patientObj.lastName = response);
     this.storage.getItem('emailId').then((response: any) => this.patientObj.emailId = response);
     this.storage.getItem('password').then((response: any) => this.patientObj.password = response);

     this.storage.getItem('dateOfBirth').then((response: any) => this.patientObj.dateOfBirth = response);
     this.storage.getItem('weight').then((response: any) => this.patientObj.weight = response);
     this.storage.getItem('height').then((response: any) => this.patientObj.height = response);
     this.storage.getItem('gender').then((response: any) => {
      this.patientObj.gender = response;
      console.log('gender is : ', response);
     });

     // get mobile number from storage
     this.storage.getItem('mobileNumber').then((response: any) => {
       this.patientObj.mobileNumber = response;
       console.log('mobile number:', this.patientObj.mobileNumber);
     });
    }

  logOutButtonClicked() {
    this.alertCtrl.create({
      header: 'Logout',
      message: 'Are you sure you want to logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'Cancel'
        },
        {
          text: 'Logout',
          role: 'Ok',
          handler: () => {
            this.storage.deleteItem('signedIn');
            this.storage.addItem('signedIn', false);
            this.storage.deleteItem('token');
            this.navCtrl.navigateRoot('sign-in');
            this.navCtrl.navigateForward('sign-in');
          }
        }
       ]
  }).then(alertEl => {
    alertEl.present();
  });
  }
}
