import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SLFeedbackRouting } from './SLFeedbackRouting.module';

import { SLFeedbackPage } from './slfeedback.page';
import { CommonVideoDisplayPage } from '../../shared/Pages/common-video-display/common-video-display.page';
import { CommonVideoDisplayPageModule } from '../../shared/Pages/common-video-display/common-video-display.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SLFeedbackRouting,
    ReactiveFormsModule,
    CommonVideoDisplayPageModule,
  ],
  declarations: [SLFeedbackPage],
  entryComponents: [CommonVideoDisplayPage]
})
export class SLFeedbackPageModule {}
