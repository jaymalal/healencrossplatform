import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SLFeedbackPage } from '../slfeedback/slfeedback.page';

const routes: Routes = [
  {
    path: '',
    component: SLFeedbackPage
  },
  {
    path: 'popup-child',
    // tslint:disable-next-line: max-line-length
    loadChildren: () => import('../../shared/Pages/common-video-display/common-video-display.module').then( m => m.CommonVideoDisplayPageModule)
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SLFeedbackRouting {}
