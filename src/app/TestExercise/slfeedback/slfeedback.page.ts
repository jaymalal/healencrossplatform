import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController } from '@ionic/angular';
import {  FormControl, Validator, FormGroup, FormBuilder} from '@angular/forms';
import { SurveyJsonManagerService } from '../../shared/Services/survey-json-manager.service';
import * as  Survey from 'survey-angular';
import { StorageService } from '../../shared/Services/storage.service';
import { Router } from '@angular/router';
import { CommonVideoDisplayPage } from '../../shared/Pages/common-video-display/common-video-display.page';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-slfeedback',
  templateUrl: './slfeedback.page.html',
  styleUrls: ['./slfeedback.page.scss'],
})
export class SLFeedbackPage implements OnInit, OnDestroy {

  // Variables
  isLoading: any;
  surveyJsonData: any;
  questionaireId: any;
  patientId: any;
  complaintId: any;
  sessionId: any;
  exerciseId: any;
  StoredTac: any;
  tac: any;
  choices: any[];
  noneId: any;
  queId: number;
  isSubmitted = false;
  que1Id: any;
  formGroup: FormGroup;

private pageSubArray: Subscription[] = [];

public set pageSub$(value: Subscription) {
  this.pageSubArray.push(value);
}

  constructor(
    private storage: StorageService,
    private fb: FormBuilder,
    private surveyJsonService: SurveyJsonManagerService,
    private router: Router,
    public modalController: ModalController
) {

  this.formGroup = this.fb.group({
    radioButton: ''
  });
 }

  ngOnInit() {

    // get Observales current values
    this.storage.getItem('patientId').then((response: any) => {
      console.log(response);
      this.patientId = response;
    });

    this.storage.getItem('complaintId').then((response: any) => {
     // console.log(response);
      this.complaintId = response;
    });

    this.storage.getItem('sessionId').then((response: any) => {
      this.sessionId = response;

    });

    this.pageSub$ = this.surveyJsonService.getExerciseId.subscribe(exerciseId => {
      this.exerciseId = exerciseId;
    });

    this.storage.getItem('tac').then((response: any) => {
      this.StoredTac = response;

      if (this.StoredTac === 'C') {
        this.tac = 'fb_sl_c';
     } else if (this.StoredTac === 'D') {
       this.tac = 'fb_sl_d';
     }

      this.pageSub$ =  this.surveyJsonService.getQuestionnaireByName(this.tac).subscribe((Response: any) => {
        this.surveyJsonData = Response.json;
        console.log('survey json data on feedback page: ', this.surveyJsonData);
        this.questionaireId = Response.id;
        console.log('questionaireId:', this.questionaireId);
        this.loadSurveyJsonAlt();
     });
    });

    setTimeout(() => {
      this.openModal(); },
  4);
  }


  async  openModal() {
    const modal = await this.modalController.create({
      component: CommonVideoDisplayPage
    });
    return await modal.present();
  }
  toggleRestrictionMode(value: any) {
    console.log(value);
  }
  loadSurveyJsonAlt() {
    const survey = new Survey.Model(this.surveyJsonData);
    survey.data = {
      self: this
    };
   // survey.onComplete.add(this.sendDataToServerAlt);
    survey.onComplete.add(() => {
      survey.clear();
    });
    const parsedJson = JSON.parse(this.surveyJsonData);
    this.choices = parsedJson.pages[0].elements[0].choices;
    console.log('choices:', this.choices);

    this.noneId = this.choices[4];
    this.que1Id = parsedJson.pages[0].elements[0].name;

    // Survey.SurveyNG.render('surveyElement1', { model: survey });
  }

  // sendDataToServerAlt(sendDataToServerAlt: any) {
  //   console.log(sendDataToServerAlt);

  //   sendDataToServerAlt.data.self.sendResponseToServerOnPMFeedback(sendDataToServerAlt.data);
  // }

  sendResponseToServerOnPMFeedback(sendDataToServerAlt: any) {
    delete sendDataToServerAlt.self;
    const dto = {
        complaintExerciseId: this.questionaireId,
        complaintId: this.complaintId,
        patientId: this.patientId,
        patientQuestionAnswer: sendDataToServerAlt,
        remedyPackageId: 1,
        sessionId: this.sessionId
    };
    this.pageSub$ = this.surveyJsonService.SavePatientAnswer(dto, this.questionaireId).subscribe(response => {
      console.log(response);
      this.router.navigate(['test-exef-list']);
    });
  }

  getResponse() {
    this.isSubmitted = true;
    let res = [...Object.values(this.formGroup.value)].filter((v) => v != null) ;
    // tslint:disable-next-line: quotemark
    res = res.filter((v) => v !== "");
    const result = {};
    result[this.que1Id] = res[0];
    // validation check for required field
    if ( res.length < 1) {
      return;
    }
    console.log('result to be passed SavePatientAns:', result);
    if (this.sessionId) {
      this.sendResponseToServerOnPMFeedback(result);
    } else {
      console.log('move for login');
    }
  }

  ngOnDestroy(): void {
    try {
      this.pageSubArray.forEach(sub => sub.unsubscribe());

    } catch (error) {
      console.log('error while unsubscribing:', error);
    }
  }
}
