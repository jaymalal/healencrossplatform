

import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { ComplaintInfoService } from '../../shared/Services/complaint-info.service';
import { StorageService } from '../../shared/Services/storage.service';
import { SurveyJsonManagerService } from '../../shared/Services/survey-json-manager.service';
import { ModalController, AlertController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { ComplaintExercise } from '../../shared/module/exercise';
import { SessionManagerService } from '../../shared/Services/session-manager.service';
import { map } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ExerciseManagerService } from 'src/app/ExerciseList/exercise-manager.service';

@Component({
  selector: 'app-test-exef-list',
  templateUrl: './test-exef-list.page.html',
  styleUrls: ['./test-exef-list.page.scss'],
})
export class TestExefListPage implements OnInit, OnDestroy {

  // Arrays
  // splExecList = [];
  // fplExecList = [];
  // tplExecList = [];
  private pageSubArray: Subscription[] = [];
  @ViewChild('video', { static: false }) videoTag: ElementRef;

  public set pageSub$(value: Subscription) {
    this.pageSubArray.push(value);
  }

  pmTestList: any = [];
  slTestList: any = [];
  pmOrderExecution = [];
  video: HTMLVideoElement;
  videoUrl: string;
  // Variables
  complaintId: any;
  videoSub$: Subscription;
  // splKey = 'b_spl';
  // tplKey = 'b_tpl';
  // fplKey = 'b_fpl';
  type = '';
  index = 0;
  patientId: any;
  chiefComplaintList: any;
  activePM = 0;
  activeSl = 0;
  constructor(private complaintInfoService: ComplaintInfoService,
    private surveyJsonService: SurveyJsonManagerService,
    private storage: StorageService,
    public modalController: ModalController,
    private nav: NavController,
    private sessionManager: SessionManagerService,
    private router: Router,
    private exerciseManager: ExerciseManagerService,
    private alertCtrl: AlertController) { }

  ngOnInit() {
    this.storage.getItem('complaintId').then((response: any) => {
       this.complaintId = response;
       console.log(this.pmOrderExecution);
      });

    this.storage.getItem('patientId').then((response: any) => {
        console.log(response);
        this.patientId = response;
        this.getPMTestList();
        this.getSLTestList();
      });

    this.pageSub$ = this.videoSub$ = this.exerciseManager.getIntroductionVideo().subscribe(
        res => {
          this.videoUrl = res;
          try {
            this.video = this.videoTag.nativeElement as HTMLVideoElement;
          } catch (e) {
            console.log('exception Handled' + e);
          }
        }
      );
  }

  getSLTestList() {

    this.pageSub$ = this.getSLTestExercises().subscribe(
      res => {
        const slExec = [];
        for (const i of res) {
          if (!i.insertExercise) {
            slExec.push(i);
          }
        }
        this.slTestList = slExec;
      }, e => {
      }
    );
  }

  getSLTestExercises(): Observable<ComplaintExercise[]> {
    return this.complaintInfoService.getAllChiefComplaintsExerciseInfoByPatientId(this.patientId).pipe(
      map(res => {
        const testList: ComplaintExercise[] = [];
        for (const i of res) {
          for (const exec of i.exercise) {
            if (exec.exercise.exef === 'SL' && exec.workflowStatus !== 'DISCONTINUE') {
              testList.push(exec);
            }
          }
        }
        return testList;
      })
    );
  }

  getFeedback(exercise: ComplaintExercise) {

    this.activePM += 1;
    this.sessionManager.activePM += 1;
    this.storage.addItem('ccId', exercise.ccId);

    this.storage.addItem('execId', exercise.id); //
    const mvd = exercise.exercise.mvd === 'Spl' ? 'spl' :
      exercise.exercise.mvd === 'Tpl' ? 'tpl' : 'fpl';

    const lj = exercise.exercise.lj.code;

    const feedbackName = lj + '_' + mvd;
    this.surveyJsonService.pmType.next(feedbackName);
    this.navigateToFeedbackForPM();
  }

  getFeedbackSLTest(exercise: ComplaintExercise) {

    this.activeSl += 1;
    this.sessionManager.activeSl += 1;
    this.storage.addItem('execId', exercise.id);

   // this.shareServ.currentExercise = exercise;

    const feedback = 'b_post_sl_feedback';
    this.surveyJsonService.pmType.next(feedback);
    this.navigateToFeedbackForTest();

  }

  getPMTestList() {

    this.pageSub$ = this.getPMTestExercise().subscribe(

      res => {
        console.log('response:', res);
        this.pmTestList = res;
      },
      e => {
        console.log('Api failed');
      }
    );
  }

getPMTestExercise(): Observable<ComplaintExercise[]> {

 return this.complaintInfoService.getAllChiefComplaintsExerciseInfoByPatientId(this.patientId).pipe(
      map(res => {
        const testList: ComplaintExercise[] = [];
        for (const i of res) {
          for (const exec of i.exercise) {
            if (exec.exercise.exef === 'PM' && exec.workflowStatus !== 'DISCONTINUE') {
              testList.push(exec);
            }
          }
        }
        return testList;
      })
    );
  }

  navigateToFeedbackForPM() {
    // this.nav.navigateForward('pmexercise-feedback');
    this.router.navigate(['pmexercise-feedback']);
  }

  navigateToFeedbackForTest() {
    this.router.navigate(['slfeedback']);
  }


CompleteButtonClicked(mvd: any, id: any, mov: any ) {

  this.surveyJsonService.pmType.next(mvd);
  this.surveyJsonService.exerciseId.next(id);
  this.nav.navigateForward('feedback-modal');
  this.index += 1;
}

testCompleted() {

  if (( this.pmTestList.length + this.slTestList.length) === (this.activePM + this.activeSl)) {
    this.activePM = 0;
    this.activeSl = 0;
    this.sessionManager.activePM = 0;
    this.sessionManager.activeSl = 0;
    this.router.navigate(['session-exercise']);
  } else {

    this.alertCtrl.create({
      header: 'Alert',
      // tslint:disable-next-line: max-line-length
      message: ('There are still ' + ( (this.pmTestList.length + this.slTestList.length) - (this.activePM + this.activeSl)) + ' tests are remain, please complete them first to proceed ahead.'),
      buttons: [
        {
          text: 'Ok',
          role: 'Ok'
        }]
  }).then(alertEl => {
    alertEl.present();
  });
  }
}

ngOnDestroy(): void {
  try {
    this.pageSubArray.forEach(sub => sub.unsubscribe());

  } catch (error) {
    console.log('error while unsubscribing:', error);

  }
}
}
