import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestExefListPage } from './test-exef-list.page';

describe('TestExefListPage', () => {
  let component: TestExefListPage;
  let fixture: ComponentFixture<TestExefListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestExefListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestExefListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
