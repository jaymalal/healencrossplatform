import { Component, OnInit, OnDestroy } from '@angular/core';
import {  FormControl, Validator, FormGroup, FormBuilder} from '@angular/forms';
import { NavController, ModalController } from '@ionic/angular';
import { SessionManagerService } from '../../shared/Services/session-manager.service';
import { SurveyJsonManagerService } from '../../shared/Services/survey-json-manager.service';
import { StorageService } from '../../shared/Services/storage.service';
import { Validators } from '@angular/forms';
import * as  Survey from 'survey-angular';
import { Router } from '@angular/router';
import { CommonVideoDisplayPage } from '../../shared/Pages/common-video-display/common-video-display.page';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-test-exef-list-feedback',
  templateUrl: './pmexercise-feedback.page.html',
  styleUrls: ['./pmexercise-feedback.page.scss'],
})

export class PMExerciseFeedbackPage implements OnInit, OnDestroy {

  isSubmitted = false;
  noRestrict: FormControl;
  que1Id: any;
  que2Id: any;
  error = false;
  queId: number;
  sessionId: any;
  execId: any;
  choices: any[];
  noneId: any;
  complaintId: any;
  patientId: any;
  endFeelMovement: FormControl;
  pmTestType: any;
  showEndMovForm = false;
  surveyJsonData: any;
  questionaireId: any;
  sec = false;
  feedback: FormGroup;
  isRestrictedMode = false;
  formGroup: FormGroup;



// tslint:disable-next-line: variable-name
private _pageSub$: Subscription[] = [];

public set pageSub$(value: Subscription) {
  this._pageSub$.push(value);
}
  constructor( private nav: NavController,
    private sessionManager: SessionManagerService,
    private storage: StorageService,
    private surveyJsonService: SurveyJsonManagerService,
    private router: Router,
    private fb: FormBuilder,
    public modalController: ModalController) {

      this.formGroup = this.fb.group({
        radioButton: '',
        radioButton2: '',
        radioButton3: ''
      });


      this.endFeelMovement = new FormControl('', Validators.required);
     }

ngOnInit() {
  this.noRestrict = new FormControl('');
  this.storage.getItem('complaintId').then((response: any) => {
      this.complaintId = response;
     });

  this.storage.getItem('sessionId').then((response: any) => {
      this.sessionId = response;
     });

  this.storage.getItem('execId').then((response: any) => {
        this.execId = response;
     });

  this.storage.getItem('patientId').then((response: any) => {
      this.patientId = response;
    });

  this.getPmTestType();

  setTimeout(() => {
    this.openModal(); },
4);

  this.formGroup.get('radioButton').valueChanges.subscribe(val => {
  console.log(val);
  this.formGroup.get('radioButton3').setValue(null);
});
  this.formGroup.get('radioButton2').valueChanges.subscribe(val => {
  console.log(val);
  this.formGroup.get('radioButton3').setValue(null);
});

}


  // ionViewDidEnter() {
  //   this.openModal();
  // }

  async  openModal() {
    const modal = await this.modalController.create({
      component: CommonVideoDisplayPage
    });
    return await modal.present();
  }

toggleRestrictionMode(value: any) {

  if (value === 'Yes') {
    this.isRestrictedMode = true;
    // this.noRestrict.reset();
  } else {
    this.isRestrictedMode = false;
  }

}

backToPmExercise() {
  this.router.navigate(['test-exef-list']);
}

getPmTestType() {
   this.pageSub$ = this.surveyJsonService.getPmType.subscribe(pmType => {
      this.pmTestType = pmType;
      this.pageSub$ = this.surveyJsonService.getQuestionnaireByName(this.pmTestType).subscribe((Response: any) => {
        this.surveyJsonData = Response.json;
        this.questionaireId = Response.id;
        console.log(this.surveyJsonData);
        const survey = new Survey.Model('' + this.surveyJsonData);
        survey.data = {
          self: this,
        };
        survey.onComplete.add(this.sendDataToServerAlt);
        survey.onComplete.add(() => {
          survey.clear();
        });
        // Survey.SurveyNG.render('surveyJsElement', { model: survey });

        const parsedJson = JSON.parse(this.surveyJsonData);
        this.choices = parsedJson.pages[0].elements[0].choices;
        console.log('choices:', this.choices);

        this.noneId = this.choices[4];
        this.que1Id = parsedJson.pages[0].elements[0].name;
        });
      }
    );
  }


sendDataToServerAlt(sendDataToServerAlt: any) {
  sendDataToServerAlt.data.self.savePatientAnswer(sendDataToServerAlt);
}


savePatientAnswer(surveyAns: any) {
  // const result = surveyAns.data;
  // delete result.self;
  const userResponse = {
    complaintExerciseId: this.execId,
    complaintId: this.complaintId,
    patientId: this.patientId,
    patientQuestionAnswer: surveyAns,
    remedyPackageId: 1,
    sessionId: this.sessionId
  };


  this.pageSub$ = this.sessionManager.saveFeedback(userResponse, this.questionaireId)
    .subscribe(
      (res: any) => {
        const execStatus = {
          cePr: 0,
          ocCounter: 0,
          postExerciseFeedbackId: res.result.id,
          progressionReps: 0,
          progressionSets: 0,
          repsBumped: true,
          repsDone: 0,
          sessionNumber: this.sessionId,
          setsBumped: true,
          setsDone: 0,
          slb: 0,
          soCounter: 0,
          soPainCounter: 0,
          status: 'DONE'
        };
        console.log(res);
        this.pageSub$ = this.sessionManager.saveExerciseStatus(userResponse, execStatus).subscribe(
          () => {
            this.router.navigate(['test-exef-list']);

          },
          e => {
            console.log(e);
            this.router.navigate(['test-exef-list']);

          }
        );
      }
    );
}

restrictReset(value: any) {
  // this.noRestrict.reset();
   this.showEndMovForm = true;

 }

restrictReset1(value: any) {
   if (value === 'none') {
     this.sec = true;
   }
   console.log(this.choices[0].text);
   // this.feedback.patchValue({ans1: this.choices[0].value, ans2: this.choices[3].value});
}
login() {
  console.log(this.formGroup);
}

getResponse() {
    this.isSubmitted = true;
    let res = [...Object.values(this.formGroup.value)].filter((v) => v != null);
    // tslint:disable-next-line: quotemark
    res = res.filter((v) => v !== "");
    const result = {};
    result[this.que1Id] = res;
    // result[this.que2Id] = this.endFeelMovement.value;
    this.error = res.length < 1;

    // validation check for required field
    if ( res.length < 1) {
      return;
    }
    console.log('result to be passed SavePatientAns:', result);
    if (this.sessionId) {
      this.savePatientAnswer(result);
    } else {
      // this.router.navigate(['user/login']).then(() => {
      //   this.popup.showErrorMessage('Please Login to continue !');
      // });
      console.log('move for login');
    }
  }


  ngOnDestroy(): void {
    try {
      this._pageSub$.forEach(sub => sub.unsubscribe());

    } catch (error) {
      console.log('error while unsubscribing:', error);
    }
  }





  reset() {
     this.formGroup.reset();
     console.log(this.formGroup.get('radioButton3'));
     this.formGroup.get('radioButton3').setValue('none');
  }
}
