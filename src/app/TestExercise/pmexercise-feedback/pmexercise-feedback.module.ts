import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { PmExerciseFBRouting } from './pmExerciseFBRouting.module';

import { PMExerciseFeedbackPage } from '../pmexercise-feedback/pmexercise-feedback.page';
import { CommonVideoDisplayPage } from '../../shared/Pages/common-video-display/common-video-display.page';
import { CommonVideoDisplayPageModule } from '../../shared/Pages/common-video-display/common-video-display.module';
import { MaterialModule } from 'src/app/material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PmExerciseFBRouting,
    CommonVideoDisplayPageModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  declarations: [PMExerciseFeedbackPage],
  entryComponents: [CommonVideoDisplayPage]
})
export class PMExerciseFeedbackPageModule {}
