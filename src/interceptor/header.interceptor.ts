import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { StorageService } from 'src/app/shared/Services/storage.service';
@Injectable()
export class HeaderInterceptor implements HttpInterceptor {
  constructor(    private storage: StorageService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const getHeaderAsync = async () => {
      try {
        const token = this.storage.getItem('token');
      

        if (!token) {
          throw new Error('Token not found');
        }
        const headers = new HttpHeaders({
          'Authorization': 'Bearer ' +  token
        });
        const authReq = req.clone({ headers });
        return await next.handle(authReq).toPromise();
      } catch (err) {
        return await next.handle(req).toPromise();
      }
    };
    return from(getHeaderAsync());
  }
}
